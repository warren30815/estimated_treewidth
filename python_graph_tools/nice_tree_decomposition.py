##  Implementation of nice tree decomposition
##  Adapted from Java version code: https://github.com/maxbannach/Jdrasil/blob/master/subprojects/core/src/main/java/jdrasil/algorithms/postprocessing/NiceTreeDecomposition.java
##  Created by 許竣翔 on 2019/12/12.
##  Copyright © 2019年 許竣翔. All rights reserved.
##  Email: qaz7821819@gmail.com

from random import *
from enum import IntEnum
from itertools import filterfalse
import networkx as nx
import matplotlib.pyplot as plt
import unittest

class BagType(IntEnum):
    LEAF = 0
    INTRODUCE = 1
    FORGET = 2
    JOIN = 3

class Stack:
    def __init__(self):
        self.items = []
        
    def isEmpty(self):
        return len(self.items)==0 
    
    def push(self, item):
        self.items.append(item)
    
    def pop(self):
        return self.items.pop() 
    
    def peek(self):
        if not self.isEmpty():
            return self.items[len(self.items)-1]
        
    def size(self):
        return len(self.items) 


def select_bag_contains_vertex_as_init_root(bag_labels_mapping, v0):
        dict_keys = bag_labels_mapping.keys()
        for key in dict_keys:
            frozenset_value = bag_labels_mapping[key]
            for value in frozenset_value:
                if v0 == value:
                    return key

class NiceDecomposeGraph():
    def __init__(self, G, v0, root, bag_labels_mapping):
        self.G = nx.Graph(G)
        self.v0 = v0
        self.root = root
        self.stack = Stack()  # stack bag labels
        self.visited = []
        self.bag_labels_mapping = bag_labels_mapping  # 由bag資訊初始化，bag index從0開始
        self.current_last_map_index = len(bag_labels_mapping) - 1  # start from 0

    def register_bag_to_labels_mapping(self, newBag):
        new_bag_index = self.current_last_map_index + 1
        self.bag_labels_mapping[new_bag_index] = newBag 
        self.current_last_map_index = new_bag_index
        return new_bag_index

    def contract_labels_mapping(self, contracted_bag_label):
        del self.bag_labels_mapping[contracted_bag_label]

    # Notice: make sure every generated new bag contains v0
    def truncate_original_root_bag(self, current_bag_label, v0):
        print("truncate_original_root_bag")
        current_bag = self.bag_labels_mapping[current_bag_label]

        if v0 not in current_bag:
            print("Error: v0 not in current_bag!!!")
            exit(-1)

        new_bag_length = len(current_bag) - 1
        only_v0_bag = frozenset([v0])
        without_v0_bag = current_bag - only_v0_bag

        if new_bag_length > 1:
            vertices = frozenset(sample(without_v0_bag, new_bag_length - 1))
            new_root_bag = frozenset.union(only_v0_bag, vertices)
            return new_root_bag
        elif new_bag_length == 1:
            new_root_bag = only_v0_bag
            return new_root_bag

    def create_leaf_bag(self, current_bag_label):
        print("create_leaf_bag")
        current_bag = self.bag_labels_mapping[current_bag_label]
        vertice = sample(current_bag, 1)[0]
        leaf_bag = frozenset([vertice])
        return leaf_bag

    def create_join_bag(self, current_bag_label):
        print("create_join_bag")
        current_bag = self.bag_labels_mapping[current_bag_label]
        left_bag = frozenset(current_bag)
        right_bag = frozenset(current_bag)
        return left_bag, right_bag

    def get_treewidth(self):
        tw = 1
        for index in self.bag_labels_mapping:
            tmp_tw = len(self.bag_labels_mapping[index]) - 1
            if (tmp_tw > tw):
                tw = tmp_tw
        return tw

    def convert_node_labels(self, first_label=0):
        G = self.G
        self.G = nx.convert_node_labels_to_integers(G, first_label=0, ordering='default', label_attribute='m')
        converted_bag_labels_mapping = {}
        new_index = 0
        for old_index in self.bag_labels_mapping:
            # update old root node index
            if old_index == self.root:
                self.root = new_index

            item = self.bag_labels_mapping[old_index]
            converted_bag_labels_mapping[new_index] = item
            new_index += 1

        self.bag_labels_mapping = converted_bag_labels_mapping

    def run(self):
        # region: First decide the real root bag (size == 1)
        while len(self.bag_labels_mapping[self.root]) > 1:
            current_root_label = self.root
            new_root_bag = self.truncate_original_root_bag(current_root_label, self.v0)
            new_root_bag_label = self.register_bag_to_labels_mapping(new_root_bag) 
            self.G.add_edge(current_root_label, new_root_bag_label)
            self.root = new_root_bag_label
        # endregion

        self.stack.push(self.root);

        while not self.stack.isEmpty():
            # print(self.stack.items)
            # print(self.G.edges)
            # print(self.bag_labels_mapping)
            # print("-----")
            bag_label = self.stack.pop()
            self.visited.append(bag_label)

            # compute number of non-visited neighbors
            unvisited_neighbor_bag_labels = [n for n in self.G[bag_label] if n not in self.visited]
            k = len(unvisited_neighbor_bag_labels)

            bag = self.bag_labels_mapping[bag_label]

            # we have no children unvisited, but we are not empty too
            if k == 0 and len(bag) > 1:
                leaf_bag = self.create_leaf_bag(bag_label)
                new_leaf_bag_index = self.register_bag_to_labels_mapping(leaf_bag) 
                self.G.add_edge(bag_label, new_leaf_bag_index)
                self.stack.push(bag_label)

            # one child -> insert introduce or forget node
            elif k == 1:
                # one child -> get it
                child_label = unvisited_neighbor_bag_labels[0]
                child_bag = self.bag_labels_mapping[child_label]

                # if it is the same, contract the edge and continue
                if bag == child_bag:
                    # if len(child_bag) > 1:
                    #     print("contracted_nodes")
                    #     self.stack.push(bag_label)
                    # else:
                    #     print("arrive at leaf")
                    self.G = nx.contracted_nodes(self.G, bag_label, child_label, self_loops=False)
                    self.contract_labels_mapping(child_label)
                    self.stack.push(bag_label)
                    self.visited.remove(bag_label)
                    continue

                # otherwise we have to reduce the distance (make introduce or forget node)
                newBag_set = set()
                delta_set = set(bag)
                delta_set -= set(child_bag)
                if len(delta_set) > 0:
                    print("create_introduce_bag")
                    newBag_set = set(bag)
                    e = next(iter(delta_set))
                    newBag_set.remove(e)
                else:
                    print("create_forget_bag")
                    delta_set = set(child_bag)
                    delta_set -= set(bag)
                    newBag_set = set(bag)
                    e = next(iter(delta_set))
                    newBag_set.add(e)
                newBag = frozenset(newBag_set)
                newBaglabel = self.register_bag_to_labels_mapping(newBag)
                self.G.remove_edge(bag_label, child_label)
                self.G.add_edge(bag_label, newBaglabel)
                self.G.add_edge(newBaglabel, child_label)
                self.stack.push(newBaglabel)

            elif k >= 2:
                left_bag, right_bag = self.create_join_bag(bag_label)
                left_bag_index = self.register_bag_to_labels_mapping(left_bag) 
                right_bag_index = self.register_bag_to_labels_mapping(right_bag)

                for n_label in unvisited_neighbor_bag_labels:
                    self.G.remove_edge(bag_label, n_label)
                self.G.add_edge(bag_label, left_bag_index)
                self.G.add_edge(bag_label, right_bag_index)

                # region: divide all neighbors into two part
                i = 0
                for n_label in unvisited_neighbor_bag_labels:
                    if i < len(unvisited_neighbor_bag_labels) / 2:
                        self.G.add_edge(left_bag_index, n_label)
                    else:
                        self.G.add_edge(right_bag_index, n_label)
                    i += 1
                # endregion

                self.stack.push(left_bag_index)
                self.stack.push(right_bag_index)

        self.convert_node_labels(first_label=0)

        return self.get_treewidth()

def draw_graph(G, root, bag_labels_mapping, attribute=True):
    pos = nx.spring_layout(G)

    if attribute:
        mapping = bag_labels_mapping
        nx.set_node_attributes(G, mapping, 'm')
        labels = nx.get_node_attributes(G, 'm')
        nx.draw(G, pos, labels=labels, node_color='g', node_size=50, with_labels=True)

    nx.draw(G, pos, node_color='g', node_size=50, with_labels=True)
    nx.draw_networkx_nodes(G, pos, nodelist=[root], node_size=300, node_color='r')
    plt.show()

def classifyBags(nice_G, root, bag_labels_mapping):
    print("classifyBags")

    stack = Stack()  # stack bag labels
    visited = []

    visited.append(root);
    stack.push(root);

    while not stack.isEmpty():
        v = stack.pop()
        friends = [n for n in nice_G[v]]

        if len(friends) == 3:
            nice_G.nodes[v]['type'] = int(BagType.JOIN)

        for friend in friends:
            if friend in visited:
                continue # parent
            if not 'type' in nice_G.nodes[v]:
                bag = bag_labels_mapping[v]
                child_bag = bag_labels_mapping[friend]
                tmp = set(bag)
                tmp -= set(child_bag)
                if len(tmp) == 1:
                    nice_G.nodes[v]['type'] = int(BagType.INTRODUCE)
                else:
                    tmp = set(child_bag)
                    tmp -= set(bag)
                    nice_G.nodes[v]['type'] = int(BagType.FORGET)
            visited.append(friend)
            stack.push(friend)

        if not 'type' in nice_G.nodes[v]:
            nice_G.nodes[v]['type'] = int(BagType.LEAF)

    # for node in nice_G.nodes:
    #     print(str(node) + "\t" + str(BagType(nice_G.nodes[node]['type'])))

def generate_k_hop_tree_decomposition(tree_decomp_G, bag_labels_mapping, v0, k_hop_nodes):
    print("generate_k_hop_tree_decomposition")
    k_hop_tree_node_list = []
    new_bag_labels_mapping = {}
    relabeled_new_bag_labels_mapping = {}
    index = 0

    stack = Stack()
    visited = []
    root = select_bag_contains_vertex_as_init_root(bag_labels_mapping, v0)
    stack.push(root)

    while not stack.isEmpty():
        bag_label = stack.pop()
        visited.append(bag_label)

        bag_set = bag_labels_mapping[bag_label]
        bag_list = list(bag_set)
        flag = False
        for x in bag_set: 
            if x not in k_hop_nodes:
                bag_list.remove(x)
            else:
                flag = True

        if (flag):
            k_hop_tree_node_list.append(bag_label)
            new_bag_labels_mapping[bag_label] = frozenset(bag_list)
            relabeled_new_bag_labels_mapping[index] = frozenset(bag_list)
            index += 1
            # compute number of non-visited neighbors
            unvisited_neighbor_bag_labels = [n for n in tree_decomp_G[bag_label] if n not in visited]
            for neighbor in unvisited_neighbor_bag_labels:
                stack.push(neighbor)

    H = tree_decomp_G.subgraph(k_hop_tree_node_list)
    if not nx.is_tree(H):
        exit(-1)

    mapping = {}
    for i, key in enumerate(new_bag_labels_mapping):
        mapping[key] = i
    relabeled_H = nx.relabel_nodes(H, mapping)
    # print(k_hop_tree_node_list)
    # print(relabeled_H.edges)
    # print(relabeled_new_bag_labels_mapping)
    # print("Done")
    return relabeled_H, 0, relabeled_new_bag_labels_mapping

def Output_k_hop_decomposition_graph(k_hop_tree_decomp, bag_labels_mapping, out_file):
    description_log = ""

    for index in bag_labels_mapping:
        item = bag_labels_mapping[index]
        description_log += "b " + str(index)
        sorted_bag = sorted(list(item))
        for vertex_in_bag in sorted_bag:
            description_log += " " + str(vertex_in_bag) 
        description_log += "\n"

    if len(k_hop_tree_decomp.edges) > 0:
        for edges in k_hop_tree_decomp.edges:
            description_log += 'e ' + str(edges[0]) + ' ' + str(edges[1]) + "\n" 
    else:
        description_log += 'e ' + "-1" + "\n"

    description_log += '-----' + "\n"

    out_file.write(description_log.encode())

def Output_nice_G_decomposition_graph(nice_G, out_file):
    description_log = ""
    G = nice_G.G
    bag_labels_mapping = nice_G.bag_labels_mapping

    for index in bag_labels_mapping:
        item = bag_labels_mapping[index]
        description_log += "b " + str(index)
        sorted_bag = sorted(list(item))
        for vertex_in_bag in sorted_bag:
            description_log += " " + str(vertex_in_bag) 
        description_log += "\n"

    if len(G.edges) > 0:
        for edges in G.edges:
            description_log += 'e ' + str(edges[0]) + ' ' + str(edges[1]) + "\n" 
    else:
        description_log += 'e ' + "-1" + "\n"

    description_log += '-----' + "\n"

    out_file.write(description_log.encode())

def Output_nice_G_bag_type(nice_G, out_file):
    description_log = ""
    G = nice_G.G
    root = nice_G.root
    bag_labels_mapping = nice_G.bag_labels_mapping
    classifyBags(G, root, bag_labels_mapping)

    description_log += "c 0:LEAF, 1:INTRODUCE, 2:FORGET, 3:JOIN\n"
    for index, vertex in enumerate(G.nodes):
        description_log += "b_type " + str(index) + " " + str(G.nodes[vertex]["type"]) + "\n"

    description_log += '-----' + "\n"
    out_file.write(description_log.encode())

def generate_nice_deGraphs_from_td_txt(inputPath, k_hop_tree_decomp_outputPath, nice_G_outputPath, nice_G_bagtype_outputPath, v0, k_hop_nodes):
    # inputPath = "../result/pace_td_format_min_degree_tree_decomposition.txt"
    # outputPath = "../result/nice_min_degree_tree_decomposition.txt"
    nice_deGraphs = []
    graph_idx = 1
    
    with open(k_hop_tree_decomp_outputPath, 'wb') as out_k_hop_tree_decomp_file, open(nice_G_outputPath, 'wb') as out_file, open(nice_G_bagtype_outputPath, 'wb') as out_type_file:
        tree_decomp_G = nx.Graph()
        with open(inputPath, 'r') as in_file:
            line = in_file.readline()
            bag_labels_mapping = {}
            while line is not None and line != "":
                if line[0] == 'c':
                    comment = line
                    out_file.write(comment.encode())
                elif line[0] == 's':
                    statistic = line.replace("\n", "").split(" ")
                    tw = statistic[3]
                    num_node = statistic[4]
                    num_edge = statistic[5]
                    description = "s td " + str(tw) + ' ' + str(num_node) + ' ' + str(num_edge) + "\n"
                    out_file.write(description.encode())
                elif line[0] == 'b':
                    line = line.replace("\n", "").split(" ")
                    bag_id = int(line[1]) # NOTICE: need to be int
                    bag_info = []
                    for vertex_index in range(2, len(line)):
                        vertex = int(line[vertex_index])
                        bag_info.append(vertex) # NOTICE: need to be int
                    bag_labels_mapping[bag_id] = frozenset(bag_info)
                elif line[0] == 'e':
                    edges = line.replace("\n", "").split(" ")
                    if edges[1] != '-1':
                        tree_decomp_G.add_edge(int(edges[1]), int(edges[2])) # NOTICE: need to be int
                    else:
                        tree_decomp_G.add_node(0)
                # run nice decomp, then write to txt, and reset graph and map dict
                elif line[0] == '-':
                    k_hop_tree_decomp, root, new_bag_labels_mapping = generate_k_hop_tree_decomposition(tree_decomp_G, bag_labels_mapping, v0, k_hop_nodes)
                    Output_k_hop_decomposition_graph(k_hop_tree_decomp, new_bag_labels_mapping, out_k_hop_tree_decomp_file)
                    # run nice decomp, and write to txt
                    # print("-----")
                    deGraph = NiceDecomposeGraph(G=k_hop_tree_decomp, v0=v0, root=root, bag_labels_mapping=new_bag_labels_mapping)
                    deGraph.run()
                    # print(deGraph.root)
                    # exit(-1)
                    nice_deGraphs.append(deGraph)
                    Output_nice_G_decomposition_graph(deGraph, out_file)
                    Output_nice_G_bag_type(deGraph, out_type_file)
                    # reset graph and map dict
                    # G = nx.Graph()
                    # bag_labels_mapping = {}
                    # print("Processed: " + str(graph_idx))
                    # graph_idx += 1

                line = in_file.readline()

    return nice_deGraphs

#if __name__ == '__main__':
    # generate_nice_deGraphs_from_td_txt()

class NiceDecomposeGraphTestCase(unittest.TestCase):
    def setUp(self):
        G = nx.Graph()
        G.add_edge(0,1)
        G.add_edge(1,2)
        G.add_edge(1,3)
        G.add_edge(3,4)
        G.add_edge(3,5)
        bag_labels_mapping = {0:frozenset(['b','c']), 1:frozenset(['b','c','g']), 2:frozenset(['b','g','a']), 3:frozenset(['c','g','e']), 4:frozenset(['g','f','e']), 5:frozenset(['e','d','c'])}
        self.deGraph = NiceDecomposeGraph(G=G, v0='g', bag_labels_mapping=bag_labels_mapping)

    def test_nice_decomp(self):
        expected = 2
        tw = self.deGraph.run()
        root = self.deGraph.root
        bag_labels_mapping = self.deGraph.bag_labels_mapping
        print("-----")
        print("node list: " + str(self.deGraph.G.nodes))
        print("edge list: " + str(self.deGraph.G.edges))
        print("root: " + str(root))
        print("bag label mapping: " + str(bag_labels_mapping))
        G = self.deGraph.G
        dfs_generator = nx.dfs_preorder_nodes(G, source=root)
        dfs_order = []
        traversal_order = []
        for order in dfs_generator:
            dfs_order.append(order)
        for reversed_order in reversed(dfs_order):
            traversal_order.append(reversed_order)
        print(traversal_order)
        classifyBags(G, root, bag_labels_mapping)
        draw_graph(G, root, bag_labels_mapping, attribute=True)
        self.assertEqual(expected, tw)

# unittest.main()