##  Implementation of FPT_baseline_algorithm
##  Created by 許竣翔 on 2019/12/25.
##  Copyright © 2019年 許竣翔. All rights reserved.
##  Email: qaz7821819@gmail.com

from tree_decomposition import Output_decomposition_graph
from nice_tree_decomposition import NiceDecomposeGraph, BagType, classifyBags, draw_graph, generate_nice_deGraphs_from_td_txt
from itertools import combinations, product
from networkx.algorithms.approximation.treewidth import treewidth_min_degree
import networkx as nx
import matplotlib.pyplot as plt
import unittest
import random
import time

INF = -1000000.0

class Maximum_total_vertex_weight():
    # 不能用set or frozenset是因為，不能讓它自動排序，不然無法掌握vertex_subset和partial_local_density_profile的順序一致
    def __init__(self, bag_index, vertex_subset, partial_local_density_profile):
        self.bag_index = bag_index
        self.vertex_subset = list(vertex_subset)  # vertex int list
        self.partial_local_density_profile = list(partial_local_density_profile)  # int list, same size with vertex_subset list
        self.value = INF

    def update_value(self, value):
        self.value = value

# def output_to_txt(message):
    # a = str(message)
    # with open("debug_log.txt", 'a') as out_file:
    #     out_file.write(str(message))
    #     out_file.write("\n")

# 搜尋插入位置，保持S和Ps list排序是對的
def searchInsert(original_list, num):
    """
    :type original_list: list[int]
    :type num: int
    :rtype: int
    """
    if len(original_list) == 0:
        return 0
    else:
        if num > original_list[len(original_list) - 1]:
            return len(original_list)
        left = 0
        right = len(original_list) - 1
        while left < right:
            mid = int(left + (right - left) / 2)
            if original_list[mid] == num: 
                return mid
            elif original_list[mid] < num:
                left = mid + 1
            else:
                right = mid
        return right

def Build_Custom_hop_Ego_Network(G, selected_node, hop):
    custom_ego = nx.ego_graph(G, selected_node, hop)
    return custom_ego

class FPT_baseline_algorithm():
    def __init__(self, original_G, nice_G, v0_root_bag_label, bag_labels_mapping):
        self.original_G = nx.Graph(original_G)
        self.nice_G = nx.Graph(nice_G)
        self.bag_labels_mapping = bag_labels_mapping  # 由bag資訊初始化，bag index從0開始
        self.root = v0_root_bag_label  
        self.traversal_order = []
        self.Ps_upper_bound_dict = {}  # key = vertex, value = this vertex all friends' edge weight 
        self.A = []  # list of Maximum_total_vertex_weight
        self.A_prime = []  # list of Maximum_total_vertex_weight

    def get_vertex_w(self, vertex):
        return float(self.original_G.nodes[vertex]["vertex_w"])

    # d上標＆d下標
    def get_vertex_d_ub_and_lb(self, vertex):
        return int(self.original_G.nodes[vertex]["d_ub"]), int(self.original_G.nodes[vertex]["d_lb"])

    def get_edge_w(self, vertex1, vertex2):
        if self.original_G.has_edge(vertex1, vertex2):
            return int(self.original_G[vertex1][vertex2]["edge_w"])
        else:
            return 0

    # leaf bag不適用
    def get_nice_G_child_bag_index_and_u(self, bagtype, bag_index):
        if bagtype == BagType.LEAF:
            print("Error: leaf bag has no child !!!!!")
            exit(-1)

        current_index = self.traversal_order.index(bag_index)

        if bagtype == BagType.INTRODUCE:
            child_bag_index = self.traversal_order[current_index - 1]  # 因為反轉dfs order了
            bag = self.bag_labels_mapping[bag_index]
            child_bag = self.bag_labels_mapping[child_bag_index]
            u_frozenset = bag - child_bag
            if len(u_frozenset) != 1:
                print("Error: find wrong child !!!!!")
                exit(-1)
            u = next(iter(u_frozenset))

            return child_bag_index, u

        elif bagtype == BagType.FORGET:
            child_bag_index = self.traversal_order[current_index - 1]  # 因為反轉dfs order了
            bag = self.bag_labels_mapping[bag_index]
            child_bag = self.bag_labels_mapping[child_bag_index]
            u_frozenset = child_bag - bag
            if len(u_frozenset) != 1:
                print("Error: find wrong child !!!!!")
                exit(-1)
            u = next(iter(u_frozenset))

            return child_bag_index, u

        elif bagtype == BagType.JOIN:
            child1_index = self.traversal_order[current_index - 1]  # 因為反轉dfs order了
            child2_index = child1_index + 1  # 因為建立bag_labels_mapping時有做過relabel處理, so each join bags' two children index will always be continuous
            return child1_index, child2_index

    def init_Ps_upper_bound_dict(self):
        original_G = self.original_G
        Ps_upper_bound_dict = self.Ps_upper_bound_dict
        for vertex in original_G:
            Ps_upper_bound_dict[vertex] = 0
            friends = [n for n in original_G[vertex]]
            for friend in friends:
                Ps_upper_bound_dict[vertex] += self.get_edge_w(vertex, friend)
            
    def get_Ps_upper_bound(self, vertex):
        Ps_upper_bound_dict = self.Ps_upper_bound_dict
        return Ps_upper_bound_dict[vertex]

    def reverse_dfs_order(self):
        nice_G = self.nice_G
        root = self.root
        traversal_order = self.traversal_order

        dfs_order = []
        dfs_generator = nx.dfs_preorder_nodes(nice_G, source=root)

        for order in dfs_generator:
            dfs_order.append(order)

        for reversed_order in reversed(dfs_order):
            traversal_order.append(reversed_order)

    # return: list of int list
    def generate_all_combinations(self, bag):
        all_combinations = []
        bag_length = len(bag)
        for length in range(0, bag_length+1):
            for item in list(combinations(bag, length)):
                all_combinations.append(list(item))

        return all_combinations

    # combination: int list
    # return: list of int list
    def generate_this_combination_all_Ps(self, combination): 
        Ps_lists = []
        ub_lists = []

        for vertex in combination:
            ub_list = range(0, self.get_Ps_upper_bound(vertex) + 1)
            ub_lists.append(ub_list)

        for Ps in product(*ub_lists):
            Ps_lists.append(list(Ps))

        return Ps_lists

    def check_is_reasonable_Ps(self, S, Ps_list):
        for i, vertex in enumerate(S):
            ub, lb = self.get_vertex_d_ub_and_lb(vertex)
            Ps = Ps_list[i]
            if Ps > ub or Ps < 0:
                return False

        return True

    def check_is_feasible_Ps(self, S, Ps_list):
        for i, vertex in enumerate(S):
            ub, lb = self.get_vertex_d_ub_and_lb(vertex)
            Ps = Ps_list[i]
            if Ps > ub or Ps < lb:
                return False

        return True

    # bag_index: int
    # vertex_subset: int list
    # partial_local_density_profile: int list
    def query_A_or_Aprime(self, prime, bag_index, vertex_subset, partial_local_density_profile, need_to_create=False):
        if len(vertex_subset) != len(partial_local_density_profile):
            print("Error: vertex_subset size is not equal to partial_local_density_profile size !!!!!")
            exit(-1)

        query_result = []
        # A
        if not prime:
            A = self.A
            query_result = [sub_A for sub_A in A if sub_A.bag_index == bag_index and sub_A.vertex_subset == vertex_subset and sub_A.partial_local_density_profile == partial_local_density_profile]
        # A'
        else:
            A_prime = self.A_prime
            query_result = [sub_A for sub_A in A_prime if sub_A.bag_index == bag_index and sub_A.vertex_subset == vertex_subset and sub_A.partial_local_density_profile == partial_local_density_profile]

        if len(query_result) == 0:
            new_item = Maximum_total_vertex_weight(bag_index, vertex_subset, partial_local_density_profile)
            
            if need_to_create:
                if not prime:
                    self.A.append(new_item)
                else:
                    self.A_prime.append(new_item)

            # print("new: isPrime: " + str(prime) + ", " + str(bag_index) + ", S: " + str(new_item.vertex_subset) + ", Ps: " + str(new_item.partial_local_density_profile) + ", value: " + str(new_item.value))
            return new_item

        elif len(query_result) == 1:
            # print("find: isPrime: " + str(prime) + ", " + str(bag_index) + ", S: " + str(query_result[0].vertex_subset) + ", Ps: " + str(query_result[0].partial_local_density_profile) + ", value: " + str(query_result[0].value))
            return query_result[0]
        else:
            print("Error: query function detect duplicate A or A' bug !!!!!")
            exit(-1)

    def get_all_child_bag_value_equal_or_larger_than_0(self, prime, child_bag_index):
        if not prime: 
            result = [a for a in self.A if a.bag_index == child_bag_index and a.value >= 0]
            return result
        else:
            result = [a for a in self.A_prime if a.bag_index == child_bag_index and a.value >= 0]
            return result

    def get_join_child_bag_value_equal_or_larger_than_0(self, child_bag_index1, child_bag_index2):
        result1 = [a for a in self.A_prime if a.bag_index == child_bag_index1 and a.value >= 0]
        result2 = [a for a in self.A_prime if a.bag_index == child_bag_index2 and a.value >= 0]
        return result1, result2

    def reverse_introduce_bag_Ps_calculation(self, Sy, Psy, u):
        Psx = []
        for i, vertex in enumerate(Sy):
            edge_w = Psy[i] + self.get_edge_w(vertex, u)
            Psx.append(edge_w)

        return Psx

    def process_leaf_bag(self, bag_index, bag):
        print("process_leaf_bag: " + str(bag))
        # output_to_txt("process_leaf_bag: " + str(bag))
        list_bag = list(bag)
        if len(list_bag) != 1:
            print("Error: leaf bag size is not 1 !!!!!")
            exit(-1)

        # region: process Ps = []
        Ps_list = []
        A = self.query_A_or_Aprime(False, bag_index, [], Ps_list, need_to_create=True)
        Aprime = self.query_A_or_Aprime(True, bag_index, [], Ps_list, need_to_create=True)
        A.update_value(0)
        Aprime.update_value(0)
        # endregion

        vertex = list_bag[0]
        ub, lb = self.get_vertex_d_ub_and_lb(vertex)
        w = self.get_vertex_w(vertex)

        # case 1
        if (ub >= 0 and 0 >= lb) or (lb == 0):
            Ps_list = [0]
            A = self.query_A_or_Aprime(False, bag_index, list_bag, Ps_list, need_to_create=True)
            Aprime = self.query_A_or_Aprime(True, bag_index, list_bag, Ps_list, need_to_create=True)
            A.update_value(w)
            Aprime.update_value(w)

        # case 2
        elif lb > 0:
            Ps_list = [0]
            Aprime = self.query_A_or_Aprime(True, bag_index, list_bag, Ps_list, need_to_create=True)
            Aprime.update_value(w)

    def process_introduce_bag(self, bag_index, bag):
        print("process_introduce_bag: " + str(bag))
        child_bag_index, u = self.get_nice_G_child_bag_index_and_u(BagType.INTRODUCE, bag_index)

        # region: A
        child_A_bags = self.get_all_child_bag_value_equal_or_larger_than_0(False, child_bag_index)
        for child_A in child_A_bags:
            # case 1
            combination = list(child_A.vertex_subset)
            Ps_list = list(child_A.partial_local_density_profile)
            A = self.query_A_or_Aprime(False, bag_index, combination, Ps_list, need_to_create=True)
            A.update_value(child_A.value)
            # case 2
            # no need to do (cuz this region query A)
        # endregion

        # region: A'
        child_Aprime_bags = self.get_all_child_bag_value_equal_or_larger_than_0(True, child_bag_index)
        for child_Aprime in child_Aprime_bags:
            # case 1
            child_combination = list(child_Aprime.vertex_subset)
            child_Ps_list = list(child_Aprime.partial_local_density_profile)
            Aprime = self.query_A_or_Aprime(True, bag_index, child_combination, child_Ps_list, need_to_create=True)
            Aprime.update_value(child_Aprime.value)
            # case 2
            combination = list(child_Aprime.vertex_subset)
            insert_index = searchInsert(combination, u)
            combination.insert(insert_index, u)

            sum_of_u_friends_edge_w = 0
            for vertex in combination:
                if vertex != u:
                    sum_of_u_friends_edge_w += self.get_edge_w(u, vertex)

            # condition (6) - must equal, or INF
            Ps_u = sum_of_u_friends_edge_w

            # Case II-(1):
            # obviously pass
            # Case II-(2):
            Ps_list = self.reverse_introduce_bag_Ps_calculation(child_combination, child_Ps_list, u)
            Ps_list.insert(insert_index, Ps_u)
            if self.check_is_feasible_Ps(combination, Ps_list):
                A = self.query_A_or_Aprime(False, bag_index, combination, Ps_list, need_to_create=True)
                A.update_value(child_Aprime.value + self.get_vertex_w(u))
            # Case II-(3):    
            # pass

            Aprime = self.query_A_or_Aprime(True, bag_index, combination, Ps_list, need_to_create=True)
            Aprime.update_value(child_Aprime.value + self.get_vertex_w(u))
        # endregion

    ### NOTICE: following code has error, please see c++ version !!!!!
    def process_forget_bag(self, bag_index, bag):
        print("process_forget_bag: " + str(bag))
        child_bag_index, u = self.get_nice_G_child_bag_index_and_u(BagType.FORGET, bag_index)

        # region: A
        child_A_bags = self.get_all_child_bag_value_equal_or_larger_than_0(False, child_bag_index)
        for child_A in child_A_bags:
            # following two will be modified later
            combination = list(child_A.vertex_subset)
            Ps_list = list(child_A.partial_local_density_profile)

            u_index = 0
            if len(combination) > 0:
                if u in combination:
                    u_index = combination.index(u)
                    combination.remove(u)
                    Ps_list.pop(u_index)
                else:
                    u_index = searchInsert(combination, u)
            # else:
            # Sy = []

            # region: compute d_lb_prime_u
            first_term = 0
            for vertex in combination:
                if u != vertex:
                    first_term += self.get_edge_w(u, vertex)

            d_ub_u, second_term = self.get_vertex_d_ub_and_lb(u)

            d_lb_prime_u = max(first_term, second_term)
            # endregion

            child_combination = list(child_A.vertex_subset)

            # OPT A (1)
            first_term_child_A = self.query_A_or_Aprime(False, child_bag_index, combination, Ps_list)
            # OPT A (2)

            max_opt_A_2_value = INF # NOTICE
            for tmp_child_A in child_A_bags:
                child_S = tmp_child_A.vertex_subset
                if child_S == child_combination:
                    if len(child_S) > 0:
                        if u in child_combination:
                            du = tmp_child_A.partial_local_density_profile[u_index]
                            if d_ub_u >= du and du >= d_lb_prime_u:
                                if tmp_child_A.value > max_opt_A_2_value:
                                    max_opt_A_2_value = tmp_child_A.value
                        # else:
                        # Sy = Sx + {u}, if Sy doesn't contain u, it is nosense
                        # pass
                    # else:
                    # Sy = Sx + {u}, if Sy = {}, it is nosense
                    # pass

            max_value = max(first_term_child_A.value, max_opt_A_2_value)
            if max_value >= 0:
                A = self.query_A_or_Aprime(False, bag_index, combination, Ps_list, need_to_create=True)
                A.update_value(max_value) 
        # endregion

        # # region: A'
        child_Aprime_bags = self.get_all_child_bag_value_equal_or_larger_than_0(True, child_bag_index)
        for child_Aprime in child_Aprime_bags:
            # following two will be modified later
            combination = list(child_Aprime.vertex_subset)
            Ps_list = list(child_Aprime.partial_local_density_profile)

            u_index = 0
            if len(combination) > 0:
                if u in combination:
                    u_index = combination.index(u)
                    combination.remove(u)
                    Ps_list.pop(u_index)
                else:
                    u_index = searchInsert(combination, u)
            # else:
            # Sy = []

            # region: compute d_lb_prime_u
            first_term = 0
            for vertex in combination:
                if u != vertex:
                    first_term += self.get_edge_w(u, vertex)

            d_ub_u, second_term = self.get_vertex_d_ub_and_lb(u)

            d_lb_prime_u = max(first_term, second_term)
            # endregion

            child_combination = list(child_Aprime.vertex_subset)

            # OPT A' (1)
            first_term_child_Aprime = self.query_A_or_Aprime(True, child_bag_index, combination, Ps_list)
            # OPT A' (2)

            max_opt_Aprime_2_value = INF # NOTICE
            for tmp_child_Aprime in child_Aprime_bags:
                child_S = tmp_child_Aprime.vertex_subset
                if child_S == child_combination:
                    if len(child_S) > 0:
                        if u in child_combination:
                            du = tmp_child_Aprime.partial_local_density_profile[u_index]
                            if d_ub_u >= du and du >= d_lb_prime_u:
                                if tmp_child_Aprime.value > max_opt_Aprime_2_value:
                                    max_opt_Aprime_2_value = tmp_child_Aprime.value
                        # else:
                        # Sy = Sx + {u}, if Sy doesn't contain u, it is nosense
                        # pass
                    # else:
                    # Sy = Sx + {u}, if Sy = {}, it is nosense
                    # pass

            max_value = max(first_term_child_Aprime.value, max_opt_Aprime_2_value)
            if max_value >= 0:
                Aprime = self.query_A_or_Aprime(True, bag_index, combination, Ps_list, need_to_create=True)
                Aprime.update_value(max_value) 
        # endregion
    ###

    def process_join_bag(self, bag_index, bag):
        print("process_join_bag: " + str(bag))
        child_bag_index1, child_bag_index2 = self.get_nice_G_child_bag_index_and_u(BagType.JOIN, bag_index)
        child_bag1 = self.bag_labels_mapping[child_bag_index1]
        child_bag2 = self.bag_labels_mapping[child_bag_index2]

        if child_bag1 != child_bag2:
            print("Error: find wrong child !!!!!")
            exit(-1)

        # region: A & A'
        child1_Aprime_bags, child2_Aprime_bags = self.get_join_child_bag_value_equal_or_larger_than_0(child_bag_index1, child_bag_index2)
        # region: preprocess and match each possible pair to reduce redundant calculation
        candidate_pair = {}
        for i, child1_Aprime in enumerate(child1_Aprime_bags):
            target_S = child1_Aprime.vertex_subset
            candidate_list = []
            for j, child2_Aprime in enumerate(child2_Aprime_bags):
                if child2_Aprime.vertex_subset == target_S:
                    candidate_list.append(j)
            candidate_pair[i] = frozenset(candidate_list)
        # endregion

        for key in candidate_pair:
            child1_Aprime = child1_Aprime_bags[key]
            target_S = child1_Aprime.vertex_subset
            Ps_prime = child1_Aprime.partial_local_density_profile

            total_vertex_w = 0
            for vertex in target_S:
                total_vertex_w += self.get_vertex_w(vertex)

            # region: 計算sum_of_edge_w_list, for check Ps
            sum_of_edge_w_list = []
            for i, parent_vertex in enumerate(target_S):
                sum_of_edge_w = 0
                for u in target_S:
                    if u != parent_vertex:
                        sum_of_edge_w += self.get_edge_w(u, parent_vertex)
                sum_of_edge_w_list.append(sum_of_edge_w)
            # endregion

            child2_Aprime_index_list = candidate_pair[key]
            for index in child2_Aprime_index_list:
                child2_Aprime = child2_Aprime_bags[index]
                Ps_prime_prime = child2_Aprime.partial_local_density_profile
                Ps = []
                for i, sum_of_edge_w in enumerate(sum_of_edge_w_list):
                    value = Ps_prime[i] + Ps_prime_prime[i] - sum_of_edge_w
                    Ps.append(value)
                tmp_value = child1_Aprime.value + child2_Aprime.value - total_vertex_w

                if self.check_is_feasible_Ps(target_S, Ps):
                    A = self.query_A_or_Aprime(False, bag_index, target_S, Ps)
                    old_A_value = A.value 
                    if tmp_value > old_A_value:
                        A = self.query_A_or_Aprime(False, bag_index, target_S, Ps, need_to_create=True)
                        A.update_value(tmp_value)

                Aprime = self.query_A_or_Aprime(True, bag_index, target_S, Ps)
                old_Aprime_value = Aprime.value 
                if tmp_value > old_Aprime_value:
                    Aprime = self.query_A_or_Aprime(True, bag_index, target_S, Ps, need_to_create=True)
                    Aprime.update_value(tmp_value)
        # endregion

    def run(self):
        self.init_Ps_upper_bound_dict()

        bag_labels_mapping = self.bag_labels_mapping
        nice_G = self.nice_G
        root = self.root

        classifyBags(nice_G, root, bag_labels_mapping)
        self.reverse_dfs_order()
        traversal_order = self.traversal_order
        # draw_graph(nice_G, root, bag_labels_mapping)
        print(traversal_order)
        # output_to_txt(traversal_order)
        print(bag_labels_mapping)
        # output_to_txt(bag_labels_mapping)

        counter = 1
        total = len(traversal_order)
        for bag_index in traversal_order:
            bag = bag_labels_mapping[bag_index] 
            bag = list(bag)
            bag.sort()
            print("Process: " + str(counter) + "/" + str(total))
            bagtype = BagType(nice_G.nodes[bag_index]['type'])

            if bagtype == BagType.LEAF:
                self.process_leaf_bag(bag_index, bag)
            elif bagtype == BagType.INTRODUCE:
                self.process_introduce_bag(bag_index, bag)
            elif bagtype == BagType.FORGET:
                self.process_forget_bag(bag_index, bag)
            elif bagtype == BagType.JOIN:
                self.process_join_bag(bag_index, bag)

            counter += 1
    def Backtrack(self, bag_index, vertex_subset, partial_local_density_profile, prime):
        '''
        A_or_Aprime: Maximum_total_vertex_weight class list
        bag_index: int
        vertex_subset: int list
        partial_local_density_profile: int list
        prime: bool
        '''
        global H

        nice_G = self.nice_G

        bagtype = nice_G.nodes[bag_index]['type']

        if bagtype == BagType.LEAF:
            # print("leaf bag_index: " + str(bag_index))
            Sx = list(vertex_subset)
            return Sx

        elif bagtype == BagType.INTRODUCE:
            child_bag_index, u = self.get_nice_G_child_bag_index_and_u(bagtype, bag_index)
            # print("introduce bag_index: " + str(bag_index))
            if u not in vertex_subset:
                # print("-----INTRODUCE u not in vertex_subset")
                # print("S: " + str(vertex_subset))
                # print("Ps: " + str(partial_local_density_profile))
                # print("-----")
                tmp_H = self.Backtrack(child_bag_index, vertex_subset, partial_local_density_profile, prime)
                set_tmp_H = set(tmp_H)
                H = H.union(set_tmp_H)
                # print("INTRODUCE u not in vertex_subset H" + str(H))
                return tmp_H
            else:
                tmp_S = list(vertex_subset)
                u_index = tmp_S.index(u)
                tmp_S.remove(u)
                Sy = tmp_S
                tmp_Ps = list(partial_local_density_profile)
                tmp_Ps.pop(u_index)
                # print("Sy: " + str(Sy))
                # print("tmp: " + str(tmp_Ps))
                Psy = []
                for i, vertex in enumerate(Sy):
                    Ps_value = tmp_Ps[i] - self.get_edge_w(vertex, u)
                    Psy.append(Ps_value)

                # print("-----INTRODUCE u in vertex_subset")
                # print("S: " + str(Sy))
                # print("Ps: " + str(Psy))
                tmp_H = self.Backtrack(child_bag_index, Sy, Psy, True)
                back_result = list(tmp_H)
                insert_index = searchInsert(back_result, u)
                back_result.insert(insert_index, u)
                # print("INTRODUCE u in vertex_subset H" + str(H))
                # print("back_result" + str(back_result))
                # print("-----")
                set_tmp_H = set(tmp_H)
                H = H.union(set_tmp_H)
                # print("INTRODUCE u in vertex_subset H" + str(H))
                return back_result

        elif bagtype == BagType.FORGET:
            # print("forget bag_index: " + str(bag_index))
            child_bag_index, u = self.get_nice_G_child_bag_index_and_u(bagtype, bag_index)
            S_type = 0

            ### preprocess: calculate S_type
            Sy = list(vertex_subset)
            insert_index = searchInsert(Sy, u)
            Sy.insert(insert_index, u)
            Psy = []

            # region: compute d_lb_prime_u
            first_term = 0
            for vertex in vertex_subset:
                if u != vertex:
                    first_term += self.get_edge_w(u, vertex)

            d_ub_u, second_term = self.get_vertex_d_ub_and_lb(u)

            d_lb_prime_u = max(first_term, second_term)
            # endregion
            # print("FORGET d_lb_prime_u" + str(d_lb_prime_u))
            # print("FORGET d_ub_u" + str(d_ub_u))
            # region: 生成所有可能的Psy
            child_Ps_lists = []
            for possible_d_u in range(d_lb_prime_u, d_ub_u + 1):
                tmp_Ps_list = list(partial_local_density_profile)
                tmp_Ps_list.insert(insert_index, possible_d_u) # Notice: need to use insert
                child_Ps_lists.append(tmp_Ps_list)
            # endregion

            if not prime:
                opt1_A = self.query_A_or_Aprime(False, child_bag_index, vertex_subset, partial_local_density_profile)
                opt1_A_value = opt1_A.value

                max_opt_A_2_value = INF # NOTICE
                max_opt_A_2_du = INF
                # print("before child_Ps_list" + str)
                for child_Ps_list in child_Ps_lists:
                    second_term_child_A = self.query_A_or_Aprime(False, child_bag_index, Sy, child_Ps_list)
                    if second_term_child_A.value > max_opt_A_2_value:
                        # print("child_Ps_list " + str(child_Ps_list))
                        max_opt_A_2_value = second_term_child_A.value
                        max_opt_A_2_du = child_Ps_list[insert_index]
                        Psy = list(child_Ps_list)

                if opt1_A_value > max_opt_A_2_value:
                    S_type = 0
                else:
                    S_type = max_opt_A_2_du

            # A'
            else:
                opt1_Aprime = self.query_A_or_Aprime(True, child_bag_index, vertex_subset, partial_local_density_profile)
                opt1_Aprime_value = opt1_Aprime.value

                max_opt_Aprime_2_value = INF # NOTICE
                max_opt_Aprime_2_du = INF
                for child_Ps_list in child_Ps_lists:
                    second_term_child_Aprime = self.query_A_or_Aprime(True, child_bag_index, Sy, child_Ps_list)
                    if second_term_child_Aprime.value > max_opt_Aprime_2_value:
                        max_opt_Aprime_2_value = second_term_child_Aprime.value
                        max_opt_Aprime_2_du = child_Ps_list[insert_index]
                        Psy = list(child_Ps_list)

                if opt1_Aprime_value > max_opt_Aprime_2_value:
                    S_type = 0
                else:
                    S_type = max_opt_Aprime_2_du
            ### preprocess done

            if S_type == 0:
                # print("-----Forget S_type == 0")
                # print("S: " + str(vertex_subset))
                # print("Ps: " + str(partial_local_density_profile))
                # print("-----")
                tmp_H = self.Backtrack(child_bag_index, vertex_subset, partial_local_density_profile, prime)
                set_tmp_H = set(tmp_H)
                H = H.union(set_tmp_H)
                return tmp_H
            else:
                # print("-----Forget S_type != 0")
                # print("S: " + str(Sy))
                # print("Ps: " + str(Psy))
                # print("-----")
                tmp_H = self.Backtrack(child_bag_index, Sy, Psy, prime)
                set_tmp_H = set(tmp_H)
                H = H.union(set_tmp_H)
                return tmp_H

        elif bagtype == BagType.JOIN:
            child_bag_index1, child_bag_index2 = self.get_nice_G_child_bag_index_and_u(bagtype, bag_index)
            Ps_prime = []
            Ps_prime_prime = []

            ### preprocess: get P' and P''
            total_vertex_w = 0
            for vertex in vertex_subset:
                total_vertex_w += self.get_vertex_w(vertex)

            upper_Ps_list = list(partial_local_density_profile)
            # region: 計算Psx + sum_of_edge_w -> upper Ps
            for i, parent_vertex in enumerate(vertex_subset):
                sum_of_edge_w = 0
                for u in vertex_subset:
                    if u != parent_vertex:
                        sum_of_edge_w += self.get_edge_w(u, parent_vertex)
                upper_Ps_list[i] += sum_of_edge_w
            # endregion

            # A
            if not prime:
                # case 1
                if self.check_is_feasible_Ps(vertex_subset, partial_local_density_profile):
                    max_A_value = max(each_A.value for each_A in self.A if each_A.bag_index == bag_index and each_A.vertex_subset == vertex_subset and each_A.partial_local_density_profile == partial_local_density_profile)

                    # region: 計算upper_Ps總共有多少組合可以組出來
                    upper_range_list = [range(i+1) for i in upper_Ps_list]
                    for tmp_Ps_prime in product(*upper_range_list): 
                        Ps_prime_list = list(tmp_Ps_prime)
                        Ps_prime_prime_list = [upper_Ps_list[i] - Ps_prime_list[i] for i in range(len(Ps_prime_list))]
                        
                        first_term_child_A = self.query_A_or_Aprime(False, child_bag_index1, vertex_subset, Ps_prime_list)
                        second_term_child_A = self.query_A_or_Aprime(False, child_bag_index2, vertex_subset, Ps_prime_prime_list)
    
                        tmp_A_value = first_term_child_A.value + second_term_child_A.value - total_vertex_w
                        if max_A_value == tmp_A_value:
                            Ps_prime = Ps_prime_list
                            Ps_prime_prime = Ps_prime_prime_list
                            break
                    # endregion
                # case 2
                # nothing need to do, cuz Ps_prime & Ps_prime_prime default value = []
            # A'
            else:
                max_Aprime_value = max(each_Aprime.value for each_Aprime in self.A_prime if each_Aprime.bag_index == bag_index and each_Aprime.vertex_subset == vertex_subset and each_Aprime.partial_local_density_profile == partial_local_density_profile)

                # region: 計算upper_Ps總共有多少組合可以組出來
                upper_range_list = [range(i+1) for i in upper_Ps_list]
                for tmp_Ps_prime in product(*upper_range_list): 
                    Ps_prime_list = list(tmp_Ps_prime)
                    Ps_prime_prime_list = [upper_Ps_list[i] - Ps_prime_list[i] for i in range(len(Ps_prime_list))]
                    
                    first_term_child_Aprime = self.query_A_or_Aprime(True, child_bag_index1, vertex_subset, Ps_prime_list)
                    second_term_child_Aprime = self.query_A_or_Aprime(True, child_bag_index2, vertex_subset, Ps_prime_prime_list)

                    tmp_Aprime_value = first_term_child_Aprime.value + second_term_child_Aprime.value - total_vertex_w
                    if max_Aprime_value == tmp_Aprime_value:
                        Ps_prime = Ps_prime_list
                        Ps_prime_prime = Ps_prime_prime_list
                        break
                # endregion
            ### end preprocess

            tmp_H1 = self.Backtrack(child_bag_index1, vertex_subset, Ps_prime, prime)
            tmp_H2 = self.Backtrack(child_bag_index2, vertex_subset, Ps_prime_prime, prime)
            set_tmp_H1 = set(tmp_H1)
            H = H.union(set_tmp_H1)
            set_tmp_H2 = set(tmp_H2)
            H = H.union(set_tmp_H2)
            H1_union_H2 = set_tmp_H1.union(set_tmp_H2)
            return H1_union_H2

        return H

class FPT_with_toy_example_TestCase(unittest.TestCase):
    def setUp(self):
        self.v0 = 10
        hop = 1

        # inputPath = "../toy_data/test_data.txt"
        inputPath = "../real_data/facebook_data.txt"
        original_G = nx.Graph()
        with open(inputPath, 'r') as file:
            line = file.readline()
            while line is not None and line != "":
                if (line[0] == 'v'):
                    nodes = line.split(" ")
                    vertex = int(nodes[1]) # NOTICE: need to be int
                    vertex_w = float(nodes[2])
                    d_lb = int(nodes[3])
                    d_ub = int(nodes[4])
                    original_G.add_node(vertex)
                    original_G.nodes[vertex]["vertex_w"] = vertex_w
                    original_G.nodes[vertex]["d_lb"] = d_lb
                    original_G.nodes[vertex]["d_ub"] = d_ub
                if (line[0] == 'e'):
                    edges = line.split(" ")
                    src = int(edges[1]) # NOTICE: need to be int
                    target = int(edges[2]) # NOTICE: need to be int
                    edge_w = int(edges[3])
                    original_G.add_edge(src, target)
                    original_G[src][target]["edge_w"] = edge_w

                line = file.readline()

        k_hop_induced_subgraph = Build_Custom_hop_Ego_Network(original_G, self.v0, hop)
        k_hop_nodes = list(k_hop_induced_subgraph.nodes)

        # inputPath = "../toy_data/test_data_tree_decomposition.txt"
        # k_hop_tree_decomp_outputPath = "../toy_data/test_data_v" + str(self.v0) + "_" + str(hop) + "hop_tree_decomposition.txt"
        # outputPath = "../toy_data/test_data_v" + str(self.v0) + "_" + str(hop) + "hop_nice_tree_decomposition.txt"
        # bagtype_output_Path = "../toy_data/test_data_v" + str(self.v0) + "_" + str(hop) + "hop_nice_tree_decomposition_bagtype.txt"
        inputPath = "../real_data/facebook_data_tree_decomposition.txt"
        k_hop_tree_decomp_outputPath = "../real_data/facebook_data_v" + str(self.v0) + "_" + str(hop) + "hop_tree_decomposition.txt"
        outputPath = "../real_data/facebook_data_v" + str(self.v0) + "_" + str(hop) + "hop_nice_tree_decomposition.txt"
        bagtype_output_Path = "../real_data/facebook_data_v" + str(self.v0) + "_" + str(hop) + "hop_nice_tree_decomposition_bagtype.txt"
        nice_deGraphs = generate_nice_deGraphs_from_td_txt(inputPath, k_hop_tree_decomp_outputPath, outputPath, bagtype_output_Path, self.v0, k_hop_nodes)
        deGraph = nice_deGraphs[0]
        root = deGraph.root
        nice_G = deGraph.G
        # NOTICE!!! vertex set data type need to use int to make sure frozenset order, cuz ['0','2'] != ['2','0']
        bag_labels_mapping = deGraph.bag_labels_mapping
        self.FPT = FPT_baseline_algorithm(original_G=original_G, nice_G=nice_G, v0_root_bag_label=root, bag_labels_mapping=bag_labels_mapping)

    def test_FPT(self):
        self.FPT.run()
        root = self.FPT.root
        bag_labels_mapping = self.FPT.bag_labels_mapping
        # print(root)
        # print(bag_labels_mapping)
        # max_Aprime_value = max(each_Aprime.value for each_Aprime in self.FPT.A_prime if each_Aprime.bag_index == 0)
        # max_Aprime_index = [i for i, x in enumerate(self.FPT.A_prime) if x.value == max_Aprime_value]
        print("---result---")
        # output_to_txt("--- v0: " + str(self.v0) + " result---")
        # print(max_Aprime_value)

        max_A_value = max(each_A.value for each_A in self.FPT.A if each_A.bag_index == 0 and self.v0 in each_A.vertex_subset)
        print("ANS: " + str(max_A_value))
        for each_A in self.FPT.A:
            if each_A.bag_index == 0 and each_A.value == max_A_value:
                print("S: " + str(each_A.vertex_subset))
                print("Ps: " + str(each_A.partial_local_density_profile))
                print("value: " + str(each_A.value))
                global H
                H = set()
                H = self.FPT.Backtrack(root, each_A.vertex_subset, each_A.partial_local_density_profile, False)
                print("H: " + str(H))
                # output_to_txt("S: " + str(each_A.vertex_subset))
                # output_to_txt("Ps: " + str(each_A.partial_local_density_profile))
                # output_to_txt("value: " + str(each_A.value))
        self.assertEqual(2, 2)

def generate_random_debug_data():
    original_outputPath = "../random_debug_data/random_debug_data.txt"
    outputPath = "../random_debug_data/random_debug_data_tree_decomposition.txt"
    original_G = nx.fast_gnp_random_graph(7, 0.5)
    num_node = original_G.number_of_nodes()
    num_edge = original_G.number_of_edges()
    tw, decomposed_tree = treewidth_min_degree(original_G)
    # print("Process: " + str(self.dataset_path) + ", " + str(graph_idx) + "/100")
    Output_decomposition_graph(decomposed_tree, outputPath, tw, num_node, num_edge)

    with open(original_outputPath, 'ab') as file:
        nx.write_edgelist(original_G, file, data=False, delimiter=' ')

    return original_G

# class FPTTestCase(unittest.TestCase):
#     def setUp(self):
#         self.v0 = 0
#         hop = 100

#         # original_G = generate_random_debug_data()
#         inputPath = "../random_debug_data/random_debug_data.txt"
#         # inputPath = "../random_debug_data/rand_data.txt"
#         original_G = nx.Graph()
#         with open(inputPath, 'r') as file:
#             line = file.readline()
#             while line is not None and line != "":
#                 if (line[0] == 'v'):
#                     nodes = line.split(" ")
#                     vertex = int(nodes[1]) # NOTICE: need to be int
#                     vertex_w = float(nodes[2])
#                     d_lb = int(nodes[3])
#                     d_ub = int(nodes[4])
#                     original_G.add_node(vertex)
#                     original_G.nodes[vertex]["vertex_w"] = vertex_w
#                     original_G.nodes[vertex]["d_lb"] = d_lb
#                     original_G.nodes[vertex]["d_ub"] = d_ub
#                 if (line[0] == 'e'):
#                     edges = line.split(" ")
#                     src = int(edges[1]) # NOTICE: need to be int
#                     target = int(edges[2]) # NOTICE: need to be int
#                     edge_w = int(edges[3])
#                     original_G.add_edge(src, target)
#                     original_G[src][target]["edge_w"] = edge_w

#                 line = file.readline()

#         k_hop_induced_subgraph = Build_Custom_hop_Ego_Network(original_G, self.v0, hop)
#         k_hop_nodes = list(k_hop_induced_subgraph.nodes)
#         k_hop_nodes.append(self.v0)

#         inputPath = "../random_debug_data/random_debug_data_tree_decomposition.txt"
#         k_hop_tree_decomp_outputPath = "../random_debug_data/random_debug_data_" + str(hop) + "hop_tree_decomposition.txt"
#         outputPath = "../random_debug_data/random_debug_data_" + str(hop) + "hop_nice_tree_decomposition.txt"
#         bagtype_output_Path = "../random_debug_data/random_debug_data_" + str(hop) + "hop_nice_tree_decomposition_bagtype.txt"
#         # inputPath = "../random_debug_data/rand_data_tree_decomposition.txt"
#         # k_hop_tree_decomp_outputPath = "../random_debug_data/rand_data_" + str(hop) + "hop_tree_decomposition.txt"
#         # outputPath = "../random_debug_data/rand_data_" + str(hop) + "hop_nice_tree_decomposition.txt"
#         # bagtype_output_Path = "../random_debug_data/rand_data_" + str(hop) + "hop_nice_tree_decomposition_bagtype.txt"
#         nice_deGraphs = generate_nice_deGraphs_from_td_txt(inputPath, k_hop_tree_decomp_outputPath, outputPath, bagtype_output_Path, self.v0, k_hop_nodes)
#         deGraph = nice_deGraphs[0]
#         root = deGraph.root
#         nice_G = deGraph.G
#         # NOTICE!!! vertex set data type need to use int to make sure frozenset order, cuz ['0','2'] != ['2','0']
#         bag_labels_mapping = deGraph.bag_labels_mapping
#         self.FPT = FPT_baseline_algorithm(original_G=original_G, nice_G=nice_G, v0_root_bag_label=root, bag_labels_mapping=bag_labels_mapping)

#     def test_FPT(self):
#         self.FPT.run()
#         root = self.FPT.root
#         max_A_value = max(each_A.value for each_A in self.FPT.A if each_A.bag_index == 0 and len(each_A.vertex_subset) > 0)
#         print("ANS: " + str(max_A_value))
#         for each_A in self.FPT.A:
#             if each_A.bag_index == 0 and each_A.value == max_A_value:
#                 print("S: " + str(each_A.vertex_subset))
#                 print("Ps: " + str(each_A.partial_local_density_profile))
#                 print("value: " + str(each_A.value))
#                 global H
#                 H = set()
#                 H = self.FPT.Backtrack(root, each_A.vertex_subset, each_A.partial_local_density_profile, False)
#                 print("H: " + str(H))
#                 # output_to_txt("S: " + str(each_A.vertex_subset))
#                 # output_to_txt("Ps: " + str(each_A.partial_local_density_profile))
#                 # output_to_txt("value: " + str(each_A.value))
#         self.assertEqual(2, 2)

unittest.main()