import time

import networkx as nx
from networkx.algorithms.approximation.treewidth import treewidth_min_degree
from networkx.utils import make_str
import numpy as np
import unittest

def Read_Graph_from_textFile(inputPath):
    graphs = []
    G = nx.Graph()
    with open(inputPath, 'r') as file:
        line = file.readline()
        while line is not None and line != "":
            if (line[0] != '-'):
                nodes = line.split(" ")
                G.add_edge(int(nodes[0]), int(nodes[1]))
            else:
                graphs.append(G)
                G = nx.Graph()
            line = file.readline()

    # if(len(graphs) == 100):
    #     print("Num Check: OK")
    #     return graphs
    # else:
    #     print("Num Check: Error")
    #     exit(-1)

def Read_Graph_from_toy_data(inputPath):
    graphs = []
    G = nx.Graph()
    with open(inputPath, 'r') as file:
        line = file.readline()
        while line is not None and line != "":
            if (line[0] != '-'):
                if (line[0] == 'e'):
                    nodes = line.split(" ")
                    G.add_edge(int(nodes[1]), int(nodes[2]))
            else:
                graphs.append(G)
                G = nx.Graph()
            line = file.readline()
            
    return graphs

def Output_dataset_info(dataset_name, hop_num, outputPath):
    with open(outputPath, 'ab') as file:
        comment_log = "c n: " + dataset_name + " h: " + str(hop_num) + "\n"
        file.write(comment_log.encode())

def Output_decomposition_graph(G, outputPath, tw, num_node, num_edge):
    with open(outputPath, 'ab') as file:
        len_bags = len(G.nodes)
        description_log = "s td " + str(len_bags) + ' ' + str(tw) + ' ' + str(num_node) + ' ' + str(num_edge) + "\n"
        for index, item in enumerate(G.nodes):
            description_log += "b " + str(index)
            for vertex_in_bag in list(item):
                description_log += " " + str(vertex_in_bag) 
            description_log += "\n"
        relabeled_G = nx.convert_node_labels_to_integers(G, first_label=0)
        if len(relabeled_G.edges) > 0:
            for edges in relabeled_G.edges:
                description_log += 'e ' + str(edges[0]) + ' ' + str(edges[1]) + "\n" 
        else:
            description_log += 'e ' + "-1" + "\n"
        description_log += '-----' + "\n"
        file.write(description_log.encode())

def main():
    hop = 1
    # datasets = ["Enron", "facebook_combined", "undirected_soc-Epinions1", "undirected_soc-LiveJournal1", "undirected_soc-pokec-relationships", "undirected_twitter_combined"]
    # datasets = ["undirected_WikiTalk", "undirected_Email-EuAll", "undirected_roadNet-PA", "undirected_roadNet-TX", "undirected_roadNet-CA", "p2p-Gnutella31", "p2p-Gnutella30", "p2p-Gnutella25", "p2p-Gnutella24", "undirected_CA-HepTh"]
    datasets = ["facebook_combined"]
    outputPath = "../result/pace_td_format_min_degree_tree_decomposition.txt"
    # make some graphs
    graphs = []
    for filename in datasets:
        datapath = "../hop_data/" + filename
        for hop_num in range(1, hop + 1):
            path = datapath + "_" + str(hop_num) + "Hop.txt"
            graphs = Read_Graph_from_textFile(path)
            # tw_sum = 0
            Output_dataset_info(filename, hop_num, outputPath)
            start_time = time.time()
            for graph_idx, eval_graph in enumerate(graphs):
                num_node = eval_graph.number_of_nodes()
                num_edge = eval_graph.number_of_edges()
                tw, decomposed_tree = treewidth_min_degree(eval_graph)
                # tw_sum += tw
                print("Process: " + str(path) + ", " + str(graph_idx) + "/100")
                # density = float(2 * num_edge) / float(num_node * (num_node - 1))
                Output_decomposition_graph(decomposed_tree, outputPath, tw, num_node, num_edge)
                # if nx.is_directed(decomposed_tree):
                #     print("88")
                #     exit()
            end_time = time.time()
            # exit()
            log = path + ", Time: " + str(end_time - start_time) + "\n"
            print(log)


# if __name__ == '__main__':
#     main()

class DecomposeGraphTestCase(unittest.TestCase):
    def setUp(self):
        # self.dataset_path = "../hop_data/undirected_roadNet-TX_1Hop.txt"
        # self.outputPath = "../result/min_degree_tree_decomposition.txt"
        # self.dataset_path = "../real_data/facebook_data.txt"
        # self.outputPath = "../real_data/facebook_data_tree_decomposition.txt"
        self.dataset_path = "../random_debug_data/rand_data.txt"
        self.outputPath = "../random_debug_data/rand_data_tree_decomposition.txt"

    # test function MUST be named with prefix "test"
    def test_run(self):
        # graphs = Read_Graph_from_textFile(self.dataset_path)
        graphs = Read_Graph_from_toy_data(self.dataset_path)
        Output_dataset_info(self.dataset_path, "all", self.outputPath)
        start_time = time.time()
        for graph_idx, eval_graph in enumerate(graphs):
            num_node = eval_graph.number_of_nodes()
            num_edge = eval_graph.number_of_edges()
            tw, decomposed_tree = treewidth_min_degree(eval_graph)
            # print("Process: " + str(self.dataset_path) + ", " + str(graph_idx) + "/100")
            Output_decomposition_graph(decomposed_tree, self.outputPath, tw, num_node, num_edge)
        end_time = time.time()
        log = self.dataset_path + ", Time: " + str(end_time - start_time) + "\n"
        print(log)
        self.assertTrue(len(graphs) > 0);

# unittest.main()