#include <iostream>
#include <vector>
#include <utility>
#include "cppitertools-master/product.hpp"
using namespace std;
using namespace iter;


int main() {
    vector<int> v1{1,2,3};
vector<int> v2{7,8};
for (auto&& [a, b] : product(v1,v2)) {
    // cout << a << ", " << b << '\n';
}
vector<int> a = {1,2,3};
for (int i : range(2,5)){
    cout << i << '\n';
}
}