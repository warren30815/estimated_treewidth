#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>
#include <vector>
#include <string>
#include <iterator>
#include <assert.h>
#include <omp.h>
#include <chrono>
#include <sstream>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/depth_first_search.hpp>
#include "cppitertools-master/enumerate.hpp"

using namespace std;
using namespace boost;

struct VertexInfo { 
    float weight;  
    int lb;
    int ub;
};

struct EdgeInfo { 
    float weight;  
};

typedef adjacency_list<vecS, vecS, undirectedS, VertexInfo, EdgeInfo> Graph;

enum class BagType
{
    LEAF,
    INTRODUCE,
    FORGET,
    JOIN
};

float INF = -1000000.0;

class Maximum_total_vertex_weight{
    private:
        int bag_index;
        vector<int> vertex_subset;
        vector<int> partial_local_density_profile;
        float value = INF;

    public:
        // constructor
        Maximum_total_vertex_weight(int _bag_index, vector<int> _vertex_subset, vector<int> _partial_local_density_profile):
            bag_index(_bag_index), vertex_subset(_vertex_subset), partial_local_density_profile(_partial_local_density_profile)
        {}

        int get_bag_index(){
            return bag_index;
        }

        vector<int> get_vertex_subset(){
            return vertex_subset;
        }

        vector<int> get_partial_local_density_profile(){
            return partial_local_density_profile;
        }

        float get_value(){
            return value;
        }

        Maximum_total_vertex_weight* get_self(){
            return this;
        }

        void update_value(float _value){
            value = _value;
        }
};

// 搜尋插入位置，保持S和Ps list排序是對的
int searchInsert(vector<int> A, int target) {
    int start = 0, end = A.size() - 1;

    if(A.size() == 0){
        return 0;
    }

    while(start <= end) {
        int mid = start + (end - start) / 2;
        if(target == A[mid]) 
            return mid;
        else if(target < A[mid])
            end = mid - 1;
        else
            start = mid + 1;
    }
    return start;
}

// python version: index() of vector
template < typename T>
pair<bool, int > findInVector(const vector<T>  & vecOfElements, const T  & element)
{
    pair<bool, int > result;
 
    // Find given element in vector
    auto it = find(vecOfElements.begin(), vecOfElements.end(), element);
 
    if (it != vecOfElements.end())
    {
        result.second = std::distance(vecOfElements.begin(), it);
        result.first = true;
    }
    else
    {
        result.first = false;
        result.second = -1;
    }
 
    return result;
}

template<class T>
void remove_vector_element(vector<T> &veci, T target){
    for(vector<int>::iterator iter=veci.begin(); iter!=veci.end(); )
    {
        if( *iter == target){
            iter = veci.erase(iter);
        }
        else
            iter ++ ;
    }
    veci.shrink_to_fit();
}

void remove_vector_element_by_index(vector<int> &veci, int index){
    veci.erase(veci.begin() + index);
    veci.shrink_to_fit();
}

class MyVisitor : public boost::default_dfs_visitor {
    public:
        MyVisitor() : vv(new vector<int>()) {}

        void discover_vertex(int v, const Graph &g) const {
            vv->push_back(v);
            return;
        }

        vector<int> &GetVector() const { return *vv; }

    private:
        boost::shared_ptr<vector<int> > vv;
};

vector<vector<int>> combinations(vector<int> src, int r) {
    vector<vector<int>> cs;
    if (r == 0) {
        return cs;
    }

    if (r == 1) {
        for (auto i = 0; i < src.size(); i++) {
            vector<int> c;
            c.push_back(src[i]);
            cs.push_back(c);
        }
        return cs;
    }
    int* places = (int*)malloc(sizeof(int) * r);
    for (auto i = 0; i < r; i++) places[i] = i;
    while (true) {
    // push_back another combination
        vector<int> c; c.reserve(r);
        for (auto i = 0; i < r; i++) {
            c.push_back(src[places[i]]);
        }
        cs.push_back(c);
        // update places
        for (auto i = 0; i < r-1; i++) {
            places[i]++;
            if (places[i+1] == places[i]) {
                places[i] = i;
                if (i == r-2) places[r-1]++;
                continue;
            }
        break;
        }
        if (places[r-1] == src.size()) break;
    }
    free(places);
    return cs;
}

// cartesian product
auto product(const vector<vector<int>>& lists) {
    vector<vector<int>> result;
    if (find_if(std::begin(lists), std::end(lists), 
        [](auto e) -> bool { return e.size() == 0; }) != std::end(lists)) {
        return result;
    }
    for (auto& e : lists[0]) {
        result.push_back({ e });
    }
    for (size_t i = 1; i < lists.size(); ++i) {
        vector<vector<int>> temp;
        for (auto& e : result) {
            for (auto f : lists[i]) {
            auto e_tmp = e;
            e_tmp.push_back(f);
            temp.push_back(e_tmp);
            }
        }
        result = temp;
    }
    return result;
}

string convert_vector_int_to_string(vector<int> v){
    std::stringstream ss;
    for(size_t i = 0; i < v.size(); ++i)
    {
        ss << v[i];
    }
    std::string s = ss.str();
    return s;
}

// region: for debug
void print_bag(vector<int> bag){
    cout << "bag: ";
    for (auto i : bag){
        cout << i << " ";
    }
    cout << endl;
}

void print_combination(vector<int> combination){
    cout << "combination: ";
    for (auto i : combination){
        cout << i << " ";
    }
    cout << endl;
}

void print_Ps(vector<int> Ps){
    cout << "Ps: ";
    for (auto i : Ps){
        cout << i << " ";
    }
    cout << endl;
}
// endregion

class FPT_baseline_algorithm{
    private:
        Graph original_G;
        Graph nice_G;
        map<int, set<int>> bag_labels_mapping;
        map<int, int> bag_type_mapping;
        int root;
        vector<int> traversal_order;
        map<int, int> Ps_upper_bound_dict;
        vector<Maximum_total_vertex_weight*> A;
        vector<Maximum_total_vertex_weight*> Aprime;

    public:
        // constructor
        FPT_baseline_algorithm(Graph _original_G, Graph _nice_G, int _v0_root_bag_label, map<int, set<int>> &_bag_labels_mapping, map<int, int> &_bag_type_mapping){
            original_G = _original_G;
            nice_G = _nice_G;
            root = _v0_root_bag_label;
            bag_labels_mapping = _bag_labels_mapping;
            bag_type_mapping = _bag_type_mapping;
        }

        vector<Maximum_total_vertex_weight*> get_A(){
            return A;
        }

        vector<Maximum_total_vertex_weight*> get_Aprime(){
            return Aprime;
        }

        float get_vertex_w(int vertex){
            return original_G[vertex].weight;
        }

        int get_total_bag_num(){
            return traversal_order.size();
        }

        // d上標＆d下標
        pair<int, int> get_vertex_d_ub_and_lb(int vertex){
            return make_pair(original_G[vertex].ub, original_G[vertex].lb);
        }

        int get_edge_w(int vertex1, int vertex2){
            boost::graph_traits<Graph>::edge_descriptor e;
            bool found;
            boost::tie(e, found) = edge(vertex1, vertex2, original_G);
            if (found){
                return original_G[e].weight;
            }else{
                return 0;
            }
        }

        // leaf bag不適用
        pair<int, int> get_nice_G_child_bag_index_and_u(BagType bagtype, int bag_index){
            // print(Error: leaf bag has no child !!!!!)
            assert (bagtype != BagType::LEAF);
                
            pair<bool, int> result = findInVector<int>(traversal_order, bag_index);
            int current_index = result.second;
            // print(Error!!: Element Not Found)
            assert (current_index != -1);
            
            if (bagtype == BagType::INTRODUCE){
                int child_bag_index = traversal_order[current_index - 1];  // 因為反轉dfs order了
                set<int> bag = bag_labels_mapping[bag_index];
                set<int> child_bag = bag_labels_mapping[child_bag_index];
                set<int> u_set;
                set_difference(bag.begin(), bag.end(), child_bag.begin(), child_bag.end(), inserter(u_set, u_set.end()));

                // cout << "Error: find wrong child !!!!!" << endl;
                assert (u_set.size() == 1);

                int u = *u_set.begin();

                return make_pair(child_bag_index, u);
            }
            else if (bagtype == BagType::FORGET){
                int child_bag_index = traversal_order[current_index - 1];  // 因為反轉dfs order了
                set<int> bag = bag_labels_mapping[bag_index];
                set<int> child_bag = bag_labels_mapping[child_bag_index];
                set<int> u_set;
                set_difference(child_bag.begin(), child_bag.end(), bag.begin(), bag.end(), inserter(u_set, u_set.end()));

                // cout << "Error: find wrong child !!!!!" << endl;
                assert (u_set.size() == 1);

                int u = *u_set.begin();

                return make_pair(child_bag_index, u);
            }
            else if (bagtype == BagType::JOIN){
                int child1_index = traversal_order[current_index - 1];  // 因為反轉dfs order了
                int child2_index = child1_index + 1;  // 因為建立bag_labels_mapping時有做過relabel處理, so each join bags' two children index will always be continuous
                return make_pair(child1_index, child2_index);
            }

            // error
            return make_pair(-1, -1);
        }

        void init_Ps_upper_bound_dict(){
            for (auto vertex : make_iterator_range(vertices(original_G))) {
                Ps_upper_bound_dict[vertex] = 0;
                auto friends = adjacent_vertices(vertex, original_G);
                for (auto v_friend : make_iterator_range(friends)){
                    Ps_upper_bound_dict[vertex] += get_edge_w(vertex, v_friend);
                }
            }
        }
        
        int get_Ps_upper_bound(int vertex){
            return Ps_upper_bound_dict[vertex];
        }

        void reverse_dfs_order(){
            MyVisitor vis;
            depth_first_search(nice_G, visitor(vis).root_vertex(root));
            vector<int> dfs_order = vis.GetVector();

            reverse(dfs_order.begin(), dfs_order.end());
            traversal_order = dfs_order;
        }

        vector<vector<int>> generate_all_combinations(vector<int> bag){
            vector<vector<int>> all_combinations;
            int bag_length = bag.size();
            vector<int> src;
            copy(bag.begin(), bag.end(), back_inserter(src));

            // NOTICE: for i = 0
            all_combinations.push_back({});

            for(int i = 1; i <= bag_length; i++){
                vector<vector<int>> result = combinations(src, i);
                for (vector<int> comb : result){
                    all_combinations.push_back(comb);
                }
            }

            return all_combinations;
        }

        vector<vector<int>> generate_this_combination_all_Ps(vector<int> combination){
            vector<vector<int>> Ps_lists;
            vector<vector<int>> ub_lists;

            for (int vertex : combination){
                int Ps_upper = get_Ps_upper_bound(vertex);
                vector<int> ub_list;
                for(int i = 0; i <= Ps_upper; i++){
                    ub_list.push_back(i);
                }
                ub_lists.push_back(ub_list);
            }

            Ps_lists = product(ub_lists);

            return Ps_lists;
        }

        bool check_is_reasonable_Ps(vector<int> S, vector<int> Ps_list){
            for (auto&& [i, vertex] : iter::enumerate(S)) {
                pair<int, int> ub_and_lb = get_vertex_d_ub_and_lb(vertex);
                int ub = ub_and_lb.first;
                int Ps = Ps_list[i];
                if (Ps > ub || Ps < 0){
                    return false;
                }
            }
            return true;
        }

        bool check_is_feasible_Ps(vector<int> S, vector<int> Ps_list){
            for (auto&& [i, vertex] : iter::enumerate(S)) {
                pair<int, int> ub_and_lb = get_vertex_d_ub_and_lb(vertex);
                int ub = ub_and_lb.first;
                int lb = ub_and_lb.second;
                int Ps = Ps_list[i];
                if (Ps > ub || Ps < lb){
                    return false;
                }
            }
            return true;
        }

        // return: the target index in A or A'
        Maximum_total_vertex_weight* query_A_or_Aprime(bool prime, int bag_index, vector<int> vertex_subset, vector<int> partial_local_density_profile, bool need_to_create){
            // print("Error: vertex_subset size is not equal to partial_local_density_profile size !!!!!")
            assert (vertex_subset.size() == partial_local_density_profile.size());

            if (!prime){
                vector<Maximum_total_vertex_weight*>::iterator it;
                it = find_if(A.begin(), A.end(), [=](Maximum_total_vertex_weight* obj){
                    return obj->get_bag_index() == bag_index && obj->get_vertex_subset() == vertex_subset && obj->get_partial_local_density_profile() == partial_local_density_profile;
                    } );

                int index = std::distance(A.begin(), it);

                if(it != A.end())
                    return A[index];
                else{
                    auto new_item = new Maximum_total_vertex_weight(bag_index, vertex_subset, partial_local_density_profile);
                    if (need_to_create){
                        A.push_back(new_item);
                    }
                    return new_item;
                }

            }
            else{
                vector<Maximum_total_vertex_weight*>::iterator it;
                it = find_if(Aprime.begin(), Aprime.end(), [=](Maximum_total_vertex_weight* obj){
                    return obj->get_bag_index() == bag_index && obj->get_vertex_subset() == vertex_subset && obj->get_partial_local_density_profile() == partial_local_density_profile;
                    } );

                int index = std::distance(Aprime.begin(), it);

                if(it != Aprime.end())
                    return Aprime[index];
                else{
                    auto new_item = new Maximum_total_vertex_weight(bag_index, vertex_subset, partial_local_density_profile);
                    if (need_to_create){
                        Aprime.push_back(new_item);
                    }
                    return new_item;
                }
            }
        }

        // function overloading for those just query children
        Maximum_total_vertex_weight* query_A_or_Aprime(vector<Maximum_total_vertex_weight*> candidates, int bag_index, vector<int> vertex_subset, vector<int> partial_local_density_profile){
            // print("Error: vertex_subset size is not equal to partial_local_density_profile size !!!!!")
            assert (vertex_subset.size() == partial_local_density_profile.size());

            vector<Maximum_total_vertex_weight*>::iterator it;
            it = find_if(candidates.begin(), candidates.end(), [=](Maximum_total_vertex_weight* obj){
                return obj->get_bag_index() == bag_index && obj->get_vertex_subset() == vertex_subset && obj->get_partial_local_density_profile() == partial_local_density_profile;
                } );

            int index = std::distance(candidates.begin(), it);

            if(it != candidates.end())
                return candidates[index];
            else{
                auto new_item = new Maximum_total_vertex_weight(bag_index, vertex_subset, partial_local_density_profile);
                return new_item;
            }
        }

        vector<Maximum_total_vertex_weight*> get_all_child_bag_value_equal_or_larger_than_0(bool prime, int child_bag_index){
            if (!prime){
                vector<Maximum_total_vertex_weight*> result;
                for (auto a : A){
                    if (a->get_bag_index() == child_bag_index && a->get_value() >= 0){
                        result.push_back(a);
                    }
                }
                return result;
            }
            else{
                vector<Maximum_total_vertex_weight*> result;
                for (auto a : Aprime){
                    if (a->get_bag_index() == child_bag_index && a->get_value() >= 0){
                        result.push_back(a);
                    }
                }
                return result;
            }
        }

        pair<vector<Maximum_total_vertex_weight*>, vector<Maximum_total_vertex_weight*>> get_join_child_bag_value_equal_or_larger_than_0(int child_bag_index1, int child_bag_index2){
            vector<Maximum_total_vertex_weight*> result1;
            vector<Maximum_total_vertex_weight*> result2;
            for (auto a : Aprime){
                vector<Maximum_total_vertex_weight*> result;
                if (a->get_bag_index() == child_bag_index1 && a->get_value() >= 0){
                    result1.push_back(a);
                }
                if (a->get_bag_index() == child_bag_index2 && a->get_value() >= 0){
                    result2.push_back(a);
                }
            }
            return make_pair(result1, result2);
        }

        vector<int> reverse_introduce_bag_Ps_calculation(vector<int> Sy, vector<int> Psy, int u){
            vector<int> Psx;
            for (auto&& [i, vertex] : iter::enumerate(Sy)) {
                int edge_w = Psy[i] + get_edge_w(vertex, u);
                Psx.push_back(edge_w);
            }
            return Psx;
        }

        // function for those undoubtedly missing query item
        void _parallel_process_private_vector(float val, vector<Maximum_total_vertex_weight*>& new_private, int bag_index, vector<int> vertex_subset, vector<int> partial_local_density_profile){
            auto new_item = new Maximum_total_vertex_weight(bag_index, vertex_subset, partial_local_density_profile);
            new_item->update_value(val);
            new_private.push_back(new_item);
        }

        void _parallel_update_new_A_or_Aprime(bool prime, vector<Maximum_total_vertex_weight*>& new_private_vector){
            if (!prime){
                A.insert(A.end(), new_private_vector.begin(), new_private_vector.end());
            }
            else{
                Aprime.insert(Aprime.end(), new_private_vector.begin(), new_private_vector.end());
            }
        }

        // function overloading for forget bag
        void _parallel_update_new_A_or_Aprime(bool prime, vector<Maximum_total_vertex_weight*>& new_private_vector, map<string, Maximum_total_vertex_weight*>& this_round_former_item_map){
            if (!prime){
                A.insert(A.end(), new_private_vector.begin(), new_private_vector.end());
            }
            else{
                Aprime.insert(Aprime.end(), new_private_vector.begin(), new_private_vector.end());
            }

            int len = new_private_vector.size();
            for (int i = 0; i < len; i++) {
                auto tmp_item = new_private_vector[i];
                int bag_index = tmp_item->get_bag_index();
                vector<int> S = tmp_item->get_vertex_subset();
                vector<int> Ps = tmp_item->get_partial_local_density_profile();
                float tmp_value = tmp_item->get_value();
                string key = to_string(bag_index) + "_" + convert_vector_int_to_string(S) + "_" + convert_vector_int_to_string(Ps);
                
                this_round_former_item_map[key] = tmp_item;
            }
        }

        // return: new item (i.e. duplicate with old will compare and update directly, won't return)
        vector<Maximum_total_vertex_weight*> _parallel_update_duplicate_A_or_Aprime(vector<Maximum_total_vertex_weight*>& new_private_vector, map<string, Maximum_total_vertex_weight*>& this_round_former_item_map){
            int len = new_private_vector.size();

            // region: deduplicate new_private_vector, only remain max value item if having same   
            map<string, Maximum_total_vertex_weight*> processed_private_map;
            for (int i = 0; i < len; i++) {
                auto tmp_item = new_private_vector[i];
                int bag_index = tmp_item->get_bag_index();
                vector<int> S = tmp_item->get_vertex_subset();
                vector<int> Ps = tmp_item->get_partial_local_density_profile();
                float tmp_value = tmp_item->get_value();
                string key = to_string(bag_index) + "_" + convert_vector_int_to_string(S) + "_" + convert_vector_int_to_string(Ps);
                
                map<string, Maximum_total_vertex_weight*>::iterator it;
                it = processed_private_map.find(key);
                if (it != processed_private_map.end()) {
                    Maximum_total_vertex_weight* old_item = it->second;
                    if (tmp_value > old_item->get_value()){
                        it->second = tmp_item;
                    }
                } 
                else {
                    processed_private_map[key] = tmp_item;
                }
            }
            // endregion

            // region: check with former section item, update A or A' value
            vector<Maximum_total_vertex_weight*> processed_private_vector;
            for (auto iter = processed_private_map.begin(); iter != processed_private_map.end(); iter++){
                string key = iter->first;
                auto tmp_item = iter->second;
                float tmp_value = tmp_item->get_value();

                map<string, Maximum_total_vertex_weight*>::iterator it;
                it = this_round_former_item_map.find(key);
                if (it != this_round_former_item_map.end()) {
                    Maximum_total_vertex_weight* old_item = it->second;
                    if (tmp_value > old_item->get_value()){
                        old_item->update_value(tmp_value);
                    }
                } 
                else {
                    processed_private_vector.push_back(iter->second);
                }
            }
            // endregion

            return processed_private_vector;
        }

        vector<Maximum_total_vertex_weight*> final_update_duplicate_A_or_Aprime(vector<Maximum_total_vertex_weight*>& final_private_vector){
            int len = final_private_vector.size();

            // region: deduplicate by key  
            map<string, Maximum_total_vertex_weight*> final_private_map;
            for (int i = 0; i < len; i++) {
                auto tmp_item = final_private_vector[i];
                int bag_index = tmp_item->get_bag_index();
                vector<int> S = tmp_item->get_vertex_subset();
                vector<int> Ps = tmp_item->get_partial_local_density_profile();
                float tmp_value = tmp_item->get_value();
                string key = to_string(bag_index) + "_" + convert_vector_int_to_string(S) + "_" + convert_vector_int_to_string(Ps);
                
                map<string, Maximum_total_vertex_weight*>::iterator it;
                it = final_private_map.find(key);
                if (it != final_private_map.end()) {
                    Maximum_total_vertex_weight* old_item = it->second;
                    if (tmp_value > old_item->get_value()){
                        it->second = tmp_item;
                    }
                } 
                else {
                    final_private_map[key] = tmp_item;
                }
            }
            // endregion

            vector<Maximum_total_vertex_weight*> processed_final_private_vector;
            for (auto iter = final_private_map.begin(); iter != final_private_map.end(); iter++){
                processed_final_private_vector.push_back(iter->second);
            }

            return processed_final_private_vector;
        }

        void process_leaf_bag(int bag_index, vector<int> bag){
            assert(bag.size() == 1);
            print_bag(bag);

            // region: process Ps = []
            auto _A = query_A_or_Aprime(false, bag_index, {}, {}, true);
            auto _Aprime = query_A_or_Aprime(true, bag_index, {}, {}, true);
            _A->update_value(0);
            _Aprime->update_value(0);
            // endregion

            int vertex = bag[0];
            pair<int, int> ub_and_lb = get_vertex_d_ub_and_lb(vertex);
            int ub = ub_and_lb.first;
            int lb = ub_and_lb.second;
            float w = get_vertex_w(vertex);

            // case 1
            if ((ub >= 0 && 0 >= lb) || (lb == 0)){
                vector<int> Ps_list = {0};
                _A = query_A_or_Aprime(false, bag_index, bag, Ps_list, true);
                _Aprime = query_A_or_Aprime(true, bag_index, bag, Ps_list, true);
                _A->update_value(w);
                _Aprime->update_value(w);
            }
            // case 2
            else if (lb > 0){
                vector<int> Ps_list = {0};
                _Aprime = query_A_or_Aprime(true, bag_index, bag, Ps_list, true);
                _Aprime->update_value(w);
            }
        }

        void process_introduce_bag(int bag_index, vector<int> bag){
            print_bag(bag);
            pair<int, int> tmp = get_nice_G_child_bag_index_and_u(BagType::INTRODUCE, bag_index);
            int child_bag_index = tmp.first;
            int u = tmp.second;

            // region: A
            auto child_A_bags = get_all_child_bag_value_equal_or_larger_than_0(false, child_bag_index);

            int n = child_A_bags.size();
            auto first = child_A_bags.begin();

            #pragma omp parallel
            {
                vector<Maximum_total_vertex_weight*> new_A_private;
                #pragma omp for
                for (int i = 0; i < n; i++) {
                    // case 1
                    auto& child_A = *(first + i);
                    vector<int> combination;
                    auto child_A_S = child_A->get_vertex_subset();
                    combination.assign(child_A_S.begin(), child_A_S.end());
                    vector<int> Ps_list;
                    auto child_A_Ps = child_A->get_partial_local_density_profile();
                    Ps_list.assign(child_A_Ps.begin(), child_A_Ps.end());
                    float child_val = child_A->get_value();
                    // cuz obviously will miss query
                    _parallel_process_private_vector(child_val, new_A_private, bag_index, combination, Ps_list);
                    // case 2
                    // no need to do (cuz this region query A)
                }
                #pragma omp critical
                {
                    _parallel_update_new_A_or_Aprime(false, new_A_private);
                }
            }
            // endregion

            // region: A'
            auto child_Aprime_bags = get_all_child_bag_value_equal_or_larger_than_0(true, child_bag_index);

            n = child_Aprime_bags.size();
            first = child_Aprime_bags.begin();

            #pragma omp parallel
            {
                vector<Maximum_total_vertex_weight*> new_A_private;
                vector<Maximum_total_vertex_weight*> new_Aprime_private;

                #pragma omp for
                for (int i = 0; i < n; i++) {
                    // case 1
                    auto& child_Aprime = *(first + i);
                    vector<int> child_combination;
                    auto child_Aprime_S = child_Aprime->get_vertex_subset();
                    child_combination.assign(child_Aprime_S.begin(), child_Aprime_S.end());
                    vector<int> child_Ps_list;
                    auto child_Aprime_Ps = child_Aprime->get_partial_local_density_profile();
                    child_Ps_list.assign(child_Aprime_Ps.begin(), child_Aprime_Ps.end());
                    float child_Aprime_val = child_Aprime->get_value();
                    // cuz obviously will miss query
                    _parallel_process_private_vector(child_Aprime_val, new_Aprime_private, bag_index, child_combination, child_Ps_list);
                    // case 2
                    vector<int> combination;
                    combination.assign(child_Aprime_S.begin(), child_Aprime_S.end());
                    int insert_index = searchInsert(combination, u);
                    combination.insert(combination.begin() + insert_index, u);
                    int sum_of_u_friends_edge_w = 0;
                    for (int vertex : combination){
                        if (vertex != u){
                            sum_of_u_friends_edge_w += get_edge_w(u, vertex);
                        }
                    }

                    // condition (6) - must equal, or INF
                    int Ps_u = sum_of_u_friends_edge_w;
                    // Case II-(1):
                    // obviously pass
                    // Case II-(2):
                    vector<int> Ps_list = reverse_introduce_bag_Ps_calculation(child_combination, child_Ps_list, u);
                    Ps_list.insert(Ps_list.begin() + insert_index, Ps_u);

                    float tmp_value = child_Aprime_val + get_vertex_w(u);
                    if (check_is_feasible_Ps(combination, Ps_list)){
                        // cuz obviously will miss query
                        _parallel_process_private_vector(tmp_value, new_A_private, bag_index, combination, Ps_list);
                    }
                    // Case II-(3):    
                    // pass

                    // cuz obviously will miss query
                    _parallel_process_private_vector(tmp_value, new_Aprime_private, bag_index, combination, Ps_list);
                }

                #pragma omp critical
                {
                    _parallel_update_new_A_or_Aprime(false, new_A_private);
                    _parallel_update_new_A_or_Aprime(true, new_Aprime_private);
                }
            }    
            // endregion

            // cout << "aprime" << endl;
            // for (auto a : Aprime){
            //     if (a->get_value() > 0 && a->get_bag_index() == bag_index){
            //         print_combination(a->get_vertex_subset());
            //         print_Ps(a->get_partial_local_density_profile());
            //         cout << a->get_value() << endl;
            //     }
            // }

            // cout << "a" << endl;
            // for (auto a : A){
            //     if (a->get_value() > 0 && a->get_bag_index() == bag_index){
            //         print_combination(a->get_vertex_subset());
            //         print_Ps(a->get_partial_local_density_profile());
            //         cout << a->get_value() << endl;
            //     }
            // }
        }

        pair<int, int> compute_forget_bag_d_lb_and_ub(vector<int> combination, int u){
            // region: compute d_lb_prime_u
            int first_term = 0;
            for (int vertex : combination){
                if (u != vertex){
                    first_term += get_edge_w(u, vertex);
                }
            }
       
            pair<int, int> ub_and_lb = get_vertex_d_ub_and_lb(u);
            int d_ub_u = ub_and_lb.first;
            int second_term = ub_and_lb.second;

            int d_lb_prime_u = max(first_term, second_term);
            // endregion

            return make_pair(d_lb_prime_u, d_ub_u);
        }

        bool forget_bag_Ps_checker(vector<int> tmp_child_Ps, vector<int> Ps_list, int u_index){
            remove_vector_element_by_index(tmp_child_Ps, u_index); // cuz tmp_child_Ps is copy vector, so remove is ok
            int len = tmp_child_Ps.size();
            for (int i = 0; i < len; i++) { 
                if (tmp_child_Ps[i] != Ps_list[i])
                    return false;
            }
            return true;
        }

        float compute_forget_bag_OPT_II_max_value(vector<Maximum_total_vertex_weight*>& child_bags_contain_u, int d_lb_prime_u, int d_ub_u, int u_index, vector<int> child_combination, vector<int> Ps_list){
            float max_opt_2_value = INF; // NOTICE
            int len = child_bags_contain_u.size();

            // just need to query contain u children (cuz we want to check OPT II)
            for (int k = 0; k < len; k++) { 
                auto tmp_child = child_bags_contain_u[k];
                auto tmp_child_S = tmp_child->get_vertex_subset();
                auto tmp_child_Ps = tmp_child->get_partial_local_density_profile();
                if (tmp_child_S == child_combination){
                    if (forget_bag_Ps_checker(tmp_child_Ps, Ps_list, u_index)){
                        int du = tmp_child_Ps[u_index];
                        if (d_ub_u >= du && du >= d_lb_prime_u){
                            if (tmp_child->get_value() > max_opt_2_value){
                                max_opt_2_value = tmp_child->get_value();
                            }
                        }
                    }
                }
            }

            return max_opt_2_value;
        }

        void process_forget_bag_helper(bool prime, vector<Maximum_total_vertex_weight*> child_bags, int u, int bag_index, int child_bag_index){
            vector<Maximum_total_vertex_weight*> child_bags_contain_u;
            vector<Maximum_total_vertex_weight*> child_bags_not_contain_u;
            map<string, Maximum_total_vertex_weight*> this_round_former_item_map;

            int original_len = child_bags.size();

            for (int i = 0; i < original_len; i++) { 
                auto child = child_bags[i];
                pair<bool, int> result = findInVector(child->get_vertex_subset(), u);
                bool found = result.first;
                if (found){
                    child_bags_contain_u.push_back(child);
                }else{
                    child_bags_not_contain_u.push_back(child);
                }
            }

            // case 1: child not contain u (but OPT II still may have value, e.g. this child = {1}, u = 2, may contain child S = {1,2})
            int not_contain_u_len = child_bags_not_contain_u.size();
            #pragma omp parallel
            {
                vector< Maximum_total_vertex_weight* > new_private;
                #pragma omp for
                for (int i = 0; i < not_contain_u_len; i++) {
                    auto child = child_bags_not_contain_u[i];
                    auto child_S = child->get_vertex_subset();
                    auto child_Ps = child->get_partial_local_density_profile();
                    vector<int> combination;
                    combination.assign(child_S.begin(), child_S.end());
                    vector<int> Ps_list;
                    Ps_list.assign(child_Ps.begin(), child_Ps.end());

                    auto d_lb_and_ub = compute_forget_bag_d_lb_and_ub(combination, u);
                    int d_lb_prime_u = d_lb_and_ub.first;
                    int d_ub_u = d_lb_and_ub.second;

                    vector<int> child_combination;
                    child_combination.assign(child_S.begin(), child_S.end());
                    int insert_index = searchInsert(combination, u);
                    child_combination.insert(child_combination.begin() + insert_index, u);
                    float max_opt_2_value = compute_forget_bag_OPT_II_max_value(child_bags_contain_u, d_lb_prime_u, d_ub_u, insert_index, child_combination, Ps_list);

                    // cuz obviously will miss query
                    float value = max(child->get_value(), max_opt_2_value);
                    _parallel_process_private_vector(value, new_private, bag_index, combination, Ps_list);
                }

                #pragma omp critical
                {
                    _parallel_update_new_A_or_Aprime(prime, new_private, this_round_former_item_map);
                }
            }
            
            // case 2: contain u
            int contain_u_len = child_bags_contain_u.size();
            vector<Maximum_total_vertex_weight*> final_private_vector;
            #pragma omp parallel
            {
                vector< Maximum_total_vertex_weight* > new_private;
                #pragma omp for
                for (int i = 0; i < contain_u_len; i++) {
                    auto child = child_bags_contain_u[i];
                    // following two will be modified later
                    vector<int> combination;
                    auto child_S = child->get_vertex_subset();
                    combination.assign(child_S.begin(), child_S.end());
                    vector<int> Ps_list;
                    auto child_Ps = child->get_partial_local_density_profile();
                    Ps_list.assign(child_Ps.begin(), child_Ps.end());

                    pair<bool, int> result = findInVector(combination, u);
                    int u_index = result.second;
                    remove_vector_element(combination, u);
                    remove_vector_element_by_index(Ps_list, u_index);
                    
                    auto d_lb_and_ub = compute_forget_bag_d_lb_and_ub(combination, u);
                    int d_lb_prime_u = d_lb_and_ub.first;
                    int d_ub_u = d_lb_and_ub.second;

                    vector<int> child_combination;
                    child_combination.assign(child_S.begin(), child_S.end());

                    // OPT A (1)
                    // just need to query not u children (cuz it's Sx)
                    auto first_term_child = query_A_or_Aprime(child_bags_not_contain_u, child_bag_index, combination, Ps_list);
                    // OPT A (2)
                    float max_opt_2_value = compute_forget_bag_OPT_II_max_value(child_bags_contain_u, d_lb_prime_u, d_ub_u, u_index, child_combination, Ps_list);

                    float max_value = max(first_term_child->get_value(), max_opt_2_value);

                    // NOTICE: It will need to handle duplicate later in critical block !!!!!
                    _parallel_process_private_vector(max_value, new_private, bag_index, combination, Ps_list);
                }

                #pragma omp critical
                {
                    vector<Maximum_total_vertex_weight*> processed_private_vector = _parallel_update_duplicate_A_or_Aprime(new_private, this_round_former_item_map);
                    final_private_vector.insert(final_private_vector.end(), processed_private_vector.begin(), processed_private_vector.end());
                }
            }
            vector<Maximum_total_vertex_weight*> processed_final_private_vector = final_update_duplicate_A_or_Aprime(final_private_vector);
            _parallel_update_new_A_or_Aprime(prime, processed_final_private_vector);
        }

        void process_forget_bag(int bag_index, vector<int> bag){
            print_bag(bag);
            pair<int, int> tmp = get_nice_G_child_bag_index_and_u(BagType::FORGET, bag_index);
            int child_bag_index = tmp.first;
            int u = tmp.second;

            // region: A
            auto child_A_bags = get_all_child_bag_value_equal_or_larger_than_0(false, child_bag_index);
            process_forget_bag_helper(false, child_A_bags, u, bag_index, child_bag_index);
            // endregion

            // region: A'
            auto child_Aprime_bags = get_all_child_bag_value_equal_or_larger_than_0(true, child_bag_index);
            process_forget_bag_helper(true, child_Aprime_bags, u, bag_index, child_bag_index);
            // endregion

            // cout << "aprime" << endl;
            // for (auto a : Aprime){
            //     if (a->get_value() > 0 && a->get_bag_index() == bag_index){
            //         print_combination(a->get_vertex_subset());
            //         print_Ps(a->get_partial_local_density_profile());
            //         cout << a->get_value() << endl;
            //     }
            // }
        }

        void process_join_bag(int bag_index, vector<int> bag){
            print_bag(bag);
            
            pair<int, int> tmp = get_nice_G_child_bag_index_and_u(BagType::JOIN, bag_index);
            int child_bag_index1 = tmp.first;
            int child_bag_index2 = tmp.second;
            auto child_bag1 = bag_labels_mapping[child_bag_index1];
            auto child_bag2 = bag_labels_mapping[child_bag_index2];

            assert(child_bag1 == child_bag2);

            // region: A & A'
            auto result = get_join_child_bag_value_equal_or_larger_than_0(child_bag_index1, child_bag_index2);
            auto child1_Aprime_bags = result.first;
            auto child2_Aprime_bags = result.second;

            // region: preprocess and match each possible pair to reduce redundant calculation
            map<int, vector<int>> candidate_pair;
            // cout << "join preprocess start" << endl;
            // cout << child1_Aprime_bags.size() << endl;
            // cout << child2_Aprime_bags.size() << endl;
            for (auto&& [i, child1_Aprime] : iter::enumerate(child1_Aprime_bags)) {
                auto target_S = child1_Aprime->get_vertex_subset();
                vector<int> candidate_list;
                for (auto&& [j, child2_Aprime] : iter::enumerate(child2_Aprime_bags)) {
                    if (child2_Aprime->get_vertex_subset() == target_S){
                        candidate_list.push_back(j);
                    }
                }
                candidate_pair[i] = candidate_list;
            }
            // cout << "join preprocess end" << endl;
            // endregion

            vector<Maximum_total_vertex_weight*> final_private_A_vector;
            vector<Maximum_total_vertex_weight*> final_private_Aprime_vector;
            int total_len = candidate_pair.size();
            #pragma omp parallel
            {
                vector< Maximum_total_vertex_weight* > new_A_private;
                vector< Maximum_total_vertex_weight* > new_Aprime_private;
                #pragma omp for
                for (int key = 0; key < total_len; key++) {
                // for (map<int, vector<int>>::iterator iter = std::begin(candidate_pair); iter != std::end(candidate_pair); iter++){
                    // int key = iter->first;
                    // vector<int> value = iter->second;
                    vector<int> value = candidate_pair.at(key);
                    auto child1_Aprime = child1_Aprime_bags[key];
                    auto target_S = child1_Aprime->get_vertex_subset();
                    auto Ps_prime = child1_Aprime->get_partial_local_density_profile();

                    float total_vertex_w = 0.0;
                    for (auto vertex_iter = target_S.begin(); vertex_iter != target_S.end(); vertex_iter++){
                        int vertex = *vertex_iter;
                        total_vertex_w += get_vertex_w(vertex);
                    }

                    // region: 計算sum_of_edge_w_list, for check Ps
                    vector<int> sum_of_edge_w_list;
                    for (auto parent_iter = target_S.begin(); parent_iter != target_S.end(); parent_iter++){
                        int parent_vertex = *parent_iter;
                        int sum_of_edge_w = 0;
                        for (auto u_iter = target_S.begin(); u_iter != target_S.end(); u_iter++){
                            int u = *u_iter;
                            if (u != parent_vertex){
                                sum_of_edge_w += get_edge_w(u, parent_vertex);
                            }
                        }
                        sum_of_edge_w_list.push_back(sum_of_edge_w);
                    }
                    // endregion

                    vector<int> child2_Aprime_index_list = candidate_pair[key];
                    int len = child2_Aprime_index_list.size();
                        
                    for (int k = 0; k < len; k++) {
                        int index = child2_Aprime_index_list[k];
                        auto child2_Aprime = child2_Aprime_bags[index];
                        auto Ps_prime_prime = child2_Aprime->get_partial_local_density_profile();
                        vector<int> Ps;
                        int i = 0;
                        for (auto sum_of_edge_w_iter = sum_of_edge_w_list.begin(); sum_of_edge_w_iter != sum_of_edge_w_list.end(); sum_of_edge_w_iter++){
                            int sum_of_edge_w = *sum_of_edge_w_iter;
                            int value = Ps_prime[i] + Ps_prime_prime[i] - sum_of_edge_w;
                            Ps.push_back(value);
                            i++;
                        }
                        float tmp_value = child1_Aprime->get_value() + child2_Aprime->get_value() - total_vertex_w;

                        if (check_is_feasible_Ps(target_S, Ps)){
                            // NOTICE: It will need to handle duplicate later in critical block !!!!!
                            _parallel_process_private_vector(tmp_value, new_A_private, bag_index, target_S, Ps);
                        }

                        if (check_is_reasonable_Ps(target_S, Ps)){
                            // NOTICE: It will need to handle duplicate later in critical block !!!!!
                            _parallel_process_private_vector(tmp_value, new_Aprime_private, bag_index, target_S, Ps);
                        }
                    }
                }
                
                #pragma omp critical
                {
                    final_private_A_vector.insert(final_private_A_vector.end(), new_A_private.begin(), new_A_private.end());
                    final_private_Aprime_vector.insert(final_private_Aprime_vector.end(), new_Aprime_private.begin(), new_Aprime_private.end());
                }
            }
            vector<Maximum_total_vertex_weight*> processed_final_private_A_vector = final_update_duplicate_A_or_Aprime(final_private_A_vector);
            vector<Maximum_total_vertex_weight*> processed_final_private_Aprime_vector = final_update_duplicate_A_or_Aprime(final_private_Aprime_vector);
            _parallel_update_new_A_or_Aprime(false, processed_final_private_A_vector);
            _parallel_update_new_A_or_Aprime(true, processed_final_private_Aprime_vector);

            // cout << "aprime" << endl;
            // for (auto a : Aprime){
            //     if (a->get_value() > 0 && a->get_bag_index() == bag_index){
            //         print_combination(a->get_vertex_subset());
            //         print_Ps(a->get_partial_local_density_profile());
            //         cout << a->get_value() << endl;
            //     }
            // }
        }

        void run(){
            cout << "run" << endl;
            init_Ps_upper_bound_dict();
            reverse_dfs_order();
            int total_len = traversal_order.size();

            for (auto&& [i, bag_index] : iter::enumerate(traversal_order)) {
                cout << "Processing " << i + 1 << " / " << total_len << "..." << endl;;
                set<int> tmp_bag = bag_labels_mapping[bag_index];
                vector<int> bag;
                bag.assign(tmp_bag.begin(), tmp_bag.end());
                // auto all_combinations = generate_all_combinations(bag);
                int bagtype_num = bag_type_mapping[bag_index];
                BagType bagtype = static_cast<BagType>(bagtype_num);

                if (bagtype == BagType::LEAF){
                    cout << "process_leaf_bag..." << endl;
                    process_leaf_bag(bag_index, bag);
                }
                else if (bagtype == BagType::INTRODUCE){
                    cout << "process_introduce_bag..." << endl;
                    process_introduce_bag(bag_index, bag);
                }
                else if (bagtype == BagType::FORGET){
                    cout << "process_forget_bag..." << endl;
                    process_forget_bag(bag_index, bag);
                }
                else if (bagtype == BagType::JOIN){
                    cout << "process_join_bag..." << endl;
                    process_join_bag(bag_index, bag);
                }
            }
            
        }
};

// 字串切割函數
vector<string> string_split(const string &s, const string &seperator){
    vector<string> result;
    typedef string::size_type string_size;
    string_size i = 0;
     
    while(i != s.size()){
        //找到字串中首個不等於分隔符的字母；
        int flag = 0;
        while(i != s.size() && flag == 0){
            flag = 1;
            for(string_size x = 0; x < seperator.size(); ++x){
                if(s[i] == seperator[x]){
                    ++i;
                    flag = 0;
                    break;
                }
            }
        }
         
        //找到又一個分隔符，將兩個分隔符之間的字串取出；
        flag = 0;
        string_size j = i;
        while(j != s.size() && flag == 0){
            for(string_size x = 0; x < seperator.size(); ++x){
                if(s[j] == seperator[x]){
                    flag = 1;
                    break;
                }
            }
            if(flag == 0) ++j;
        }
        if(i != j){
            result.push_back(s.substr(i, j-i));
            i = j;
        }
    }
    return result;
}

Graph Preprocess_original_G(string path){
    ifstream file(path);  // ifstream: Stream class to read from files
    Graph graph;
    vector<VertexInfo> VertexList;
    vector<EdgeInfo> EdgeList;
    vector<pair<int,int>> EPairList;

    cout << "Preprocessing original_G..." << endl;
    if (file.is_open()) {
        string line;
        while (getline(file, line)) {
            if (line[0] == 'c'){
                // pass
                // it is comment log
            }
            else if (line[0] == 'd'){
                vector<string> description = string_split(line, " ");
                int vertices_num = stoi(description[1]);
                int edges_num = stoi(description[2]);
                VertexList = vector<VertexInfo>(vertices_num);
            }
            else if (line[0] == 'v'){
                vector<string> vertex_data = string_split(line, " ");
                int v = stoi(vertex_data[1]);
                float vertex_w = stof(vertex_data[2]);
                int vertex_lb = stoi(vertex_data[3]);
                int vertex_ub = stoi(vertex_data[4]);
                VertexInfo vi;
                vi.weight = vertex_w;
                vi.lb = vertex_lb;
                vi.ub = vertex_ub;
                VertexList[v] = vi;
            }
            else if (line[0] == 'e'){
                vector<string> edge_data = string_split(line, " ");
                int src = stoi(edge_data[1]);
                int target = stoi(edge_data[2]);
                float edge_w = stof(edge_data[3]);
                EdgeInfo ei;
                ei.weight = edge_w;
                EPairList.push_back(pair<int,int>(src, target));
                EdgeList.push_back(ei);
            }
            // 以'-'來分隔一組Graph data
            else if (line[0] == '-'){
                // todo
            }
        }
        file.close(); 
    }

    for (auto &&vi : VertexList)
    {
        add_vertex(vi, graph);
    }
    
    for(int i = 0; i < EPairList.size(); i++){
        int src = EPairList[i].first, target = EPairList[i].second;
        add_edge(src, target, EdgeList[i], graph);
    }

    cout << "Preprocessing Done" << endl; 
    return graph;
}

Graph Preprocess_nice_G(string nice_G_path, string bagtype_path, map<int, set<int>> &bag_labels_mapping, map<int, int> &bag_type_mapping){
    ifstream file(nice_G_path);  // ifstream: Stream class to read from files
    ifstream bagtype_file(bagtype_path);  // ifstream: Stream class to read from files
    Graph graph;

    cout << "Preprocessing nice_G..." << endl;
    if (file.is_open()) {
        string line;
        while (getline(file, line)) {
            if (line[0] == 'c'){
                // pass
                // it is comment log
            }
            else if (line[0] == 's'){
                // vector<string> description = string_split(line, " ");
                // int vertices_num = stoi(description[1]);
                // int edges_num = stoi(description[2]);
            }
            else if (line[0] == 'b'){
                vector<string> bag = string_split(line, " ");
                int bag_size = bag.size();
                int bag_index = stoi(bag[1]);
                set<int> bag_info;
                for (int i = 2; i < bag_size; i++){
                    int vertex_index = stoi(bag[i]);
                    bag_info.insert(vertex_index);
                }
                bag_labels_mapping[bag_index] = bag_info;

                add_vertex(graph);
            }
            else if (line[0] == 'e'){
                vector<string> edge_data = string_split(line, " ");
                int src = stoi(edge_data[1]);
                int target = stoi(edge_data[2]);
                add_edge(src, target, graph);
            }
            // 以'-'來分隔一組Graph data
            else if (line[0] == '-'){
                // todo
            }
        }
        file.close(); 
    }

    cout << "Preprocessing nice_G bagtype..." << endl;
    if (bagtype_file.is_open()) {
        string line;
        while (getline(bagtype_file, line)) {
            if (line[0] == 'b'){
                vector<string> bag = string_split(line, " ");
                int bag_index = stoi(bag[1]);
                int bag_type = stoi(bag[2]);
                bag_type_mapping[bag_index] = bag_type;
            }
            // 以'-'來分隔一組data
            else if (line[0] == '-'){
                // todo
            }
        }
        bagtype_file.close(); 
    }

    cout << "Preprocessing Done" << endl; 
    return graph;
}

int main(int argc, const char * argv[]) {
    int v0 = 28;  // NOTICE
    int hop = 1;  // NOTICE
    // string original_G_path = "../toy_data/test_data.txt";
    // string nice_G_path = "../toy_data/test_data_v" + std::to_string(v0) + "_" + std::to_string(hop) + "hop_nice_tree_decomposition.txt";
    // string bagtype_path = "../toy_data/test_data_v" + std::to_string(v0) + "_" + std::to_string(hop) + "hop_nice_tree_decomposition_bagtype.txt";
    // string original_G_path = "../random_debug_data/random_debug_data.txt";
    // string nice_G_path = "../random_debug_data/random_debug_data_100hop_nice_tree_decomposition.txt";
    // string bagtype_path = "../random_debug_data/random_debug_data_100hop_nice_tree_decomposition_bagtype.txt";
    // string original_G_path = "../random_debug_data/rand_data.txt";
    // string nice_G_path = "../random_debug_data/rand_data_" + std::to_string(hop) + "hop_nice_tree_decomposition.txt";
    // string bagtype_path = "../random_debug_data/rand_data_" + std::to_string(hop) + "hop_nice_tree_decomposition_bagtype.txt";
    string original_G_path = "../real_data/facebook_data.txt";
    string nice_G_path = "../real_data/facebook_data_v" + std::to_string(v0) + "_" + std::to_string(hop) + "hop_nice_tree_decomposition.txt";
    string bagtype_path = "../real_data/facebook_data_v" + std::to_string(v0) + "_" + std::to_string(hop) + "hop_nice_tree_decomposition_bagtype.txt";
    map<int, set<int>> bag_labels_mapping;
    map<int, int> bag_type_mapping;
    Graph original_G = Preprocess_original_G(original_G_path);
    Graph nice_G = Preprocess_nice_G(nice_G_path, bagtype_path, bag_labels_mapping, bag_type_mapping);

    using clock = std::chrono::steady_clock;
    clock::time_point start = clock::now();

    FPT_baseline_algorithm fpt(original_G, nice_G, 0, bag_labels_mapping, bag_type_mapping);
    fpt.run();
    auto A = fpt.get_A();
    float max_value = INF;
    int max_index = -1;

    clock::time_point end = clock::now();
    cout << "Total elapsed time (not including preprocessing time): " << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " sec."<< endl;

    Maximum_total_vertex_weight* ans;
    for (auto&& [i, a] : iter::enumerate(A)) {
        float tmp = a->get_value();

        if (a->get_bag_index() == 0 && tmp > max_value){
            bool found;
            pair<bool, int> result = findInVector(a->get_vertex_subset(), v0);
            found = result.first;
            if (found){
                max_value = tmp;
                max_index = i;
                ans = a;
            }
        }
    }

    cout << "------- statistics: -------" << endl;
    cout << "original graph: " << endl;
    cout << "node num: " << num_vertices(original_G) << endl;
    cout << "edge num: " << num_edges(original_G) << endl;
    cout << "v0: " << to_string(v0) << endl;
    cout << "hop: " << to_string(hop) << endl;
    cout << "nice tree decomp: " << endl;
    cout << "total_bag_num: " << fpt.get_total_bag_num() << endl;

    cout << "------- result: -------" << endl;
    print_combination(ans->get_vertex_subset());
    print_Ps(ans->get_partial_local_density_profile());
    cout << max_value << endl;

    return 0;
}
