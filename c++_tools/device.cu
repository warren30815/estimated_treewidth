#include <thrust/device_vector.h>
#include "device.h"

using namespace std;

void kernel(int bag_index, vector<Maximum_total_vertex_weight*>& child_A_bags, FPT_baseline_algorithm* tmp_fpt)
{
    unsigned int index = blockDim.x * blockIdx.x + threadIdx.x;
    auto child_A = child_A_bags[index];
    auto child_A_S = child_A->get_vertex_subset();
    int child_A_S_size = child_A_S.size();
    thrust::device_vector<int> combination(child_A_S.begin(), child_A_S.begin() + child_A_S_size);
    auto child_A_Ps = child_A->get_partial_local_density_profile();
    int child_A_Ps_size = child_A_Ps.size();
    thrust::device_vector<int> Ps_list(child_A_Ps.begin(), child_A_Ps.begin() + child_A_Ps_size);
    auto _A = tmp_fpt->query_A_or_Aprime(false, bag_index, combination, Ps_list, true);
    _A->update_value(child_A->get_value());
    // positions[index] = index; // use this one for debugging the index
}
