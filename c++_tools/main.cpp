#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/subgraph.hpp>
#include <boost/graph/graph_utility.hpp>

using namespace std;
using namespace boost;

struct VertexInfo { 
    float weight;  
    int lb;
    int ub;
};

struct EdgeInfo { 
    float weight;  
};

typedef adjacency_list<vecS, vecS, undirectedS, VertexInfo, EdgeInfo> Graph;

// region test case 1
const int v0 = 0;
const int k = 3;
const float opt_w = 3.9;
int a[4] = {0,1,3,4};
vector<int> H(a, a+4);
// endregion

// // region test case 2
// int v0 = 6;
// int k = 3;
// float opt_w = 4.8;
// int a[5] = {1,2,3,5,6};
// vector<int> H(a, a+5);
// // endregion

// 字串切割函數
vector<string> string_split(const string &s, const string &seperator){
    vector<string> result;
    typedef string::size_type string_size;
    string_size i = 0;
     
    while(i != s.size()){
        //找到字串中首個不等於分隔符的字母；
        int flag = 0;
        while(i != s.size() && flag == 0){
            flag = 1;
            for(string_size x = 0; x < seperator.size(); ++x){
                if(s[i] == seperator[x]){
                    ++i;
                    flag = 0;
                    break;
                }
            }
        }
         
        //找到又一個分隔符，將兩個分隔符之間的字串取出；
        flag = 0;
        string_size j = i;
        while(j != s.size() && flag == 0){
            for(string_size x = 0; x < seperator.size(); ++x){
                if(s[j] == seperator[x]){
                    flag = 1;
                    break;
                }
            }
            if(flag == 0) ++j;
        }
        if(i != j){
            result.push_back(s.substr(i, j-i));
            i = j;
        }
    }
    return result;
}

int main(int argc, const char * argv[]) {
    string path = "toy_data/test_data.txt";
    ifstream file(path);  // ifstream: Stream class to read from files
    Graph graph;

    cout << "Preprocessing..." << endl;
    if (file.is_open()) {
        string line;
        while (getline(file, line)) {
            if (line[0] == 'c'){
                // pass
                // it is comment log
            }
            else if (line[0] == 'd'){
                vector<string> description = string_split(line, " ");
                int vertices_num = stoi(description[1]);
                int edges_num = stoi(description[2]);
                
            }
            else if (line[0] == 'v'){
                vector<string> vertex_data = string_split(line, " ");
                float vertex_w = stof(vertex_data[2]);
                int vertex_lb = stoi(vertex_data[3]);
                int vertex_ub = stoi(vertex_data[4]);
                VertexInfo vi;
                vi.weight = vertex_w;
                vi.lb = vertex_lb;
                vi.ub = vertex_ub;
                add_vertex(vi, graph);
            }
            else if (line[0] == 'e'){
                vector<string> edge_data = string_split(line, " ");
                int src = stoi(edge_data[1]);
                int target = stoi(edge_data[2]);
                float edge_w = stof(edge_data[3]);
                EdgeInfo ei;
                ei.weight = edge_w;
                add_edge(src, target, ei, graph);
            }
            // 以'-'來分隔一組Graph data
            else if (line[0] == '-'){
                // todo
            }
        }
        file.close(); 
    }

    cout << "Preprocessing Done" << endl; 

    return 0;
}

/// Region: some boost graph library usage example

// // print出所有vertex的property
// for (auto v : make_iterator_range(vertices(graph))) {
//     cout << "Vertex " << v << ", ";
//     cout << "weight " << graph[v].weight << ", ";
//     cout << "lb " << graph[v].lb << ", ";
//     cout << "ub " << graph[v].ub << "\n";
// }

// // print出所有edge的property
// for (auto e : make_iterator_range(edges(graph))) {
//     auto s = source(e, graph);
//     auto t = target(e, graph);
//     cout << "Edge " << s << " -> " << t << " (weight = " << graph[e].weight << ")\n";
// } 

// // 查看vertex的info(property)
// cout << graph[0].weight << endl;  // 0.7
// cout << graph[0].lb << endl;  // 1
// cout << graph[0].ub << endl;  // 4

// // 查看邊和點的num
// cout << num_vertices(graph) << endl;  // 7
// cout << num_edges(graph) << endl;  // 9

// // print出vertex 1所有的鄰居
// auto neighbors = adjacent_vertices(1, graph);
// for (auto vd : make_iterator_range(neighbors))
//     cout << "1 has adjacent vertex " << vd << "\n"; 

// // 查看某vertex的degree
// cout << "Degree of vertex 1: " << degree(1, graph) << endl;  // 3

// // 刪除vertex 1的所有邊，但vertex 1仍會存在在vertices(graph)中
// clear_vertex(1, graph);

// // 刪除0跟1之間的edge
// remove_edge(0, 1, graph);

/// Endregion

/// Region: How to extract induced subgraph
// for subgraph, edge_index_t tag is necessary
// typedef subgraph< adjacency_list< vecS, vecS, directedS, VertexInfo, property<edge_index_t, int, EdgeInfo> > > SubGraph;
// const int N = 6;
// SubGraph G0(N);

// SubGraph& G1 = G0.create_subgraph();
// SubGraph& G2 = G0.create_subgraph();

// add_vertex(1, G1); // global vertex C becomes local A1 for G1
// add_vertex(2, G1); // global vertex E becomes local B1 for G1
// add_vertex(4, G1); // global vertex F becomes local C1 for G1

// add_edge(0, 5, G0);
// add_edge(1, 2, G0);
// add_edge(1, 4, G0);
// add_edge(2, 4, G0);
// add_edge(5, 3, G0);

// for (auto v : make_iterator_range(vertices(G1))) {
//     cout << "Vertex " << G1.local_to_global(v) << ", ";
//     cout << "weight " << G1[v].weight << ", ";
//     cout << "lb " << G1[v].lb << ", ";
//     cout << "ub " << G1[v].ub << "\n";
// }

// for (auto e : make_iterator_range(edges(G1))) {
//     auto s = source(e, graph);
//     auto t = target(e, graph);
//     cout << "Edge " << G1.local_to_global(s) << " -> " << G1.local_to_global(t) << "\n";
// }

/// Endregion

/// Region: How to get vertex induced subgraph

// Notice: For subgraph, edge_index_t tag is necessary!!!

// typedef subgraph< adjacency_list< vecS, vecS, directedS, VertexInfo, property<edge_index_t, int, EdgeInfo> > > SubGraph;
// const int N = 6;
// SubGraph G0(N);

// SubGraph& G1 = G0.create_subgraph();

// auto v1 = add_vertex(1, G1); 
// G1[v1].weight = 2;
// auto v2 = add_vertex(2, G1); 
// G1[v2].lb = 4;
// auto v3 = add_vertex(4, G1);
// G1[v3].ub = 6; 

// add_edge(0, 5, G0);
// add_edge(1, 2, G0);
// add_edge(1, 4, G0);
// add_edge(2, 4, G0);
// add_edge(5, 3, G0);

// for (auto v : make_iterator_range(vertices(G1))) {
//     cout << "Vertex " << G1.local_to_global(v) << ", ";
//     cout << "weight " << G1[v].weight << ", ";
//     cout << "lb " << G1[v].lb << ", ";
//     cout << "ub " << G1[v].ub << "\n";
// }

// for (auto e : make_iterator_range(edges(G1))) {
//     auto s = source(e, graph);
//     auto t = target(e, graph);
//     cout << "Edge " << G1.local_to_global(s) << " -> " << G1.local_to_global(t) << "\n";
// }

/// Endregion

/// Region: Boost DFS example on an undirected graph.

// #include <boost/graph/adjacency_list.hpp>
// #include <boost/graph/depth_first_search.hpp>
// #include <iostream>
// using namespace std;

// typedef boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS> MyGraph;
// typedef boost::graph_traits<MyGraph>::vertex_descriptor MyVertex;

// class MyVisitor : public boost::default_dfs_visitor
// {
// public:
//     void discover_vertex(MyVertex v, const MyGraph& g) const
//     {
//         cerr << v << " ";
//         return;
//     }
// };

// int main()
// {
//     MyGraph g;
//     boost::add_edge(0, 1, g);
//     boost::add_edge(0, 2, g);
//     boost::add_edge(0, 4, g);
//     boost::add_edge(4, 5, g);
//     boost::add_edge(1, 3, g);

//     MyVisitor vis;
//     boost::depth_first_search(g, boost::visitor(vis).root_vertex(2));
//     // output: 2 0 1 3 4 5
//     return 0;
// }

/// Endregion